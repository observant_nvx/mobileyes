<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    protected $fillable = [
		'answer_status_id',
	];

	public static $rules = array(
		'answer_status_id' => 'required',
		);

	// public function scopeGroupByUserTaskgroup($query) {
 //        return $query->select('user_id', 'tasks.taskgroup_id')->join('tasks', 'answers.task_id', '=', 'tasks.id')->groupBy('tasks.taskgroup_id')->groupBy('user_id');
 //    }

 //    public function scopeGetByTaskgroup($query, $taskgroup_id, $user_id) {
 //        return $query->select('answers.id','answers.user_id','answers.task_id','answers.answer_status_id','answers.data','tasks.tasktype_id','tasks.taskgroup_id','tasks.description','tasks.money')->join('tasks', 'answers.task_id', '=', 'tasks.id')->where('tasks.taskgroup_id', $taskgroup_id)->where('answers.user_id', $user_id);
 //    }
}
