<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'name', 'address', 'contact_data', 'payment_data', 'type', 'is_active',
    ];

    public static $rules = array(
        'name' => 'required|min:5',
        );

    public function taskgroups()
    {
        return $this->hasMany('App\Taskgroup');
    }

    public static function scopeActive($query)
    {   
        return $query->where('is_active', true);
    }
    

    public function getStatusTextAttribute()
    {
        return $this->attributes['is_active'] ? 'active' : 'inactive'; 
    }
}
