<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;

use App\Taskgroup;
use App\Task;
use App\User;
use App\Registration;

use DB;

class Helper
{
	public static function getTaskgroupImage($image)
	{
		if(preg_match('|^http(s)?://[a-z0-9-]+(.[a-z0-9-]+)*(:[0-9]+)?(/.*)?$|i', $image)) {
			return $image;
		} else {
			return asset('/images/taskgroups/'.$image);
		}
	}

	public static function getTaskType($tasktype_id)
	{
		return DB::table('task_types')->where('id', $tasktype_id)->value('type');
	}

	public static function getTaskGroup($taskgroup_id)
	{
		$taskgroup = Taskgroup::findOrFail($taskgroup_id);
		return $taskgroup->name;
	}

	public static function getUser($user_id)
	{
		$user = User::getProfile($user_id)->first();
		return $user->name.' '.$user->surname. ' ('.$user->email.')';
	}

	public static function getUserProfile($user_id)
	{
		$profile = User::getProfile($user_id)->first();
		return $profile;
	}

	public static function getLocationFromRegistration($registration_id)
	{
		$registration = Registration::with('location')->findOrFail($registration_id);
		return $registration->location;
	}

	public static function getProfileFromAnswer($user_id)
	{
		$user = User::with('profile')->findOrFail($user_id);
		return $user->profile;
	}

	public static function getBank($bank_id)
	{
		return DB::table('banks')->where('id', $bank_id)->value('name');
	}

	public static function getTaskQuestion($id)
	{
		$task = Task::findOrFail($id);
		$task['description'] = json_decode($task['description']);
		return $task['description']->question;
	}

	public static function getRegistrationStatusById($id)
	{
		return DB::table('registration_statuses')->where('id', $id)->value('status');
	}
}
