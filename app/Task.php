<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Task extends Model
{
	protected $fillable = [
	'tasktype_id', 'taskgroup_id', 'order', 'description', 'money', 'is_active',
	];


	public static $rules = array(
		'tasktype_id' => 'required',
		'taskgroup_id'=>'required',
		'order'=>'required',
		'money'=>'required',
		'is_active'=>'required',
		);

	
    public function getStatusTextAttribute()
    {
        return $this->attributes['is_active'] ? 'active' : 'inactive'; 
    }

	public static function sortArray($data, $field)
	{
		if(!is_array($field)) $field = array($field);
		usort($data, function($a, $b) use($field) {
			$retval = 0;
			foreach($field as $fieldname) {
				if($retval == 0) $retval = strnatcmp($a[$fieldname],$b[$fieldname]);
			}
			return $retval;
		});
		return $data;
	}
}
