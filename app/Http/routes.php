<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::auth();

Route::group(['middleware' => ['auth']], function()
{
	Route::resource('admin/users', 'UsersController');
	Route::get('admin/taskgroups/images/{id}', 'TaskgroupsController@images');
	Route::get('admin/taskgroups/locations/{id}', 'TaskgroupsController@locations');
	Route::get('admin/taskgroups/duplicate', 'TaskgroupsController@duplicate');
	Route::post('admin/taskgroups/post_duplicate', 'TaskgroupsController@post_duplicate');
	Route::post('admin/taskgroups/status/{id}', 'TaskgroupsController@status');
	Route::resource('admin/taskgroups', 'TaskgroupsController');
	Route::resource('admin/locations', 'LocationsController');
	Route::resource('admin/tasks', 'TasksController');
	Route::resource('admin/answers', 'AnswersController');
	Route::resource('admin/registrations', 'RegistrationsController');
	Route::resource('admin/clients', 'ClientsController');
	Route::resource('admin/globalvars', 'GlobalVarsController');

	/* individual routes */
	Route::post('admin/locations/bulk', 'LocationsController@bulk_import');
	Route::get('admin/locations/results/{locations}', 'LocationsController@results');
	Route::get('admin/quality/edit/{id}/{status?}', 'QualityController@edit');
	Route::get('admin/quality', 'QualityController@index');
	Route::get('admin/quality/{id}/{status?}', 'QualityController@show');
	Route::post('admin/quality/{id}/{status?}', 'QualityController@update');

	Route::get('admin/payment/edit/{id}/{status?}', 'PaymentController@edit');
	Route::post('admin/payment/bulk/{status}', 'PaymentController@bulk');
	Route::get('admin/payment/{status?}/{group?}', 'PaymentController@index');
	Route::post('admin/payment/{id}/{status?}', 'PaymentController@update');
	

});