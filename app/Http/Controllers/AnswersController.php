<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Answer;
use View;
use Input;
use Validator;
use Redirect;
use DB;

class AnswersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $answers = Answer::groupByUserTaskgroup()->get();
        return View::make('admin.answers.index', compact('answers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Input::all();
        foreach ($input['answer_status_id'] as $key => $value) {
            $answer = Answer::findOrFail($key);
            $update = array('answer_status_id'=>$value);
            $answer->update($update);
        }
        return Redirect::route('admin.answers.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $explode = explode('_', $id);
        $answers = Answer::getByTaskgroupUser($explode[0], $explode[1])->get()->toArray();
        array_walk($answers, function (&$item) {
            $item['description']=json_decode($item['description']);
            if(isset($item['description']->answers)) {
                $tmp = array();
                array_walk($item['description']->answers, function ($one) use (&$tmp){                    
                    $tmp[$one->value] = $one->text;
                });
                $item['description']->answers = $tmp;
            }
        });
        $answer_statuses = [''=> 'Please select'] + DB::table('answer_statuses')->lists('status','id');
        return View::make('admin.answers.edit', compact('answers','answer_statuses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
