<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Registration;
use App\Task;
use App\Answer;
use View;
use DB;
use Input;
use Validator;
use Redirect;

class PaymentController extends Controller
{
	public function index($status=null, $group=null)
	{
		$statuses = DB::table('answer_statuses')->whereIn('id', [11,12,13])->get();
		$status_ids = $status ? DB::table('answer_statuses')->where('status', $status)->pluck('id') : [11,12,13];
		if($group) {
			$registrations = Registration::forPaymentProccessGroup($status_ids)->with('taskgroup')->with('location')->with('user')->get();
		} else {
			$registrations = Registration::forPaymentProccess($status_ids)->with('taskgroup')->with('location')->with('user')->get();
		}
		foreach ($registrations as $registration) {
			$registration_status = DB::table('answer_statuses')->where('id', $registration->answers_status)->value('status');
			$registration->answers_status = $registration_status;
		}
		return View::make('admin.payment.index', compact('registrations', 'status', 'group', 'statuses'));
	}

	public function edit($id, $status=null)
	{
		$status_ids = $status ? DB::table('answer_statuses')->where('status', $status)->pluck('id') : [11,12,13];
		$registration = Registration::with(['answers' => function ($query) use ($status_ids) {
			$query->whereIn('answer_status_id', $status_ids);
		}])->with('taskgroup')->with('location')->with('user')->with('profile')->findOrFail($id);
		foreach ($registration->answers as $answer) {
			$task = Task::where('id', $answer->task_id)->first();
			$answer->money = $task['money'];
			$answer_money[] = $task['money'];
			$task['description'] = json_decode($task['description']);
			$answer->description = $task['description'];
			$answer_statuses[] = $answer->answer_status_id;
		}
		$registration->total_money = array_sum($answer_money);
		$registration->answer_status = min($answer_statuses);
		$statuses = [''=> 'Please select'] + DB::table('answer_statuses')->where('id', [$registration->answer_status+1])->lists('status','id');
		return View::make('admin.payment.edit', compact('registration', 'status', 'statuses'));
	}

	public function update(Request $request, $id, $status=null)
	{
		$input = Input::all();
		foreach ($input['answer_status_id'] as $key => $value) {
			$answer = Answer::findOrFail($key);
			$update = array('answer_status_id'=>$input['status']);
			$answer->update($update);
		}
		return Redirect::action('PaymentController@index', [$status]);
	}

	public function bulk(Request $request, $status)
	{
		$input = Input::all();
		$status_id  = DB::table('answer_statuses')->where('status', $status)->pluck('id');
		if($input['user_id']) {
			$new_status = $status_id[0]+1;
			foreach ($input['user_id'] as $user_id) {
				$registrations = Registration::forPaymentProccessGroup($status_id)->where('answers.user_id', $user_id)->first();
				$answer_ids = explode(',', $registrations->answers);
				foreach ($answer_ids as $answer_id) {
					$answer = Answer::findOrFail($answer_id);
					$update = array('answer_status_id'=>$new_status);
					$answer->update($update);
				}
			}
		}
		return Redirect::action('PaymentController@index', [$status]);
	}
}
