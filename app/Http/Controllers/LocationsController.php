<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Location;
use App\Registration;
use View;
use DB;
use Input;
use Alert;
use Validator;
use Redirect;

class LocationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $locations = Location::all();
        return View::make('admin.locations.index', compact('locations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $location = new Location;
        return View::make('admin.locations.create', compact('location'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Input::all();
        $input['latitude'] = number_format($input['latitude'], 8, '.', '');
        $input['longitude'] = number_format($input['longitude'], 8, '.', '');
        $input['is_active'] = isset($input['is_active']) && $input['is_active'] == 'on' ? true : false;
        $validation = Validator::make($input, Location::$rules);
        if ($validation->passes())
        {
            Location::create($input);
            return Redirect::route('admin.locations.index');
        }
        return Redirect::route('admin.locations.create')
        ->withInput()
        ->withErrors($validation);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $location = Location::findOrFail($id);
        $previous = Location::where('id' ,'<', $id)->orderBy('id', 'desc')->first();
        $next = Location::where('id' ,'>', $id)->orderBy('id', 'asc')->first();
        return View::make('admin.locations.edit', compact('location', 'previous', 'next'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $location = Location::findOrFail($id);
        $input = Input::all();
        $input['is_active'] = isset($input['is_active']) && $input['is_active'] == 'on' ? true : false;
        $validation = Validator::make($input, Location::$rules);
        if ($validation->passes())
        {
            $location->update($input);
            return Redirect::route('admin.locations.index');
        }
        return Redirect::route('admin.locations.edit', [$location])
        ->withInput()
        ->withErrors($validation);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $location = Location::find($id);
        $checkreg = Registration::where('location_id', $id)->first();
        if(!$checkreg) {
            DB::table('location_taskgroup')->where('location_id', $location->id)->delete();
            $location->delete();
            alert()->success('Location deleted successfully');
        } else {
            $location->update(array('is_active' => false));
            alert()->warning('Location could not be deleted', 'Made inactive instead');
        }
        return Redirect::route('admin.locations.index');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function bulk_import(Request $request)
    {
        $input = Input::all();
        $validation = Validator::make($input, array('csv'=>'required'));
        if ($validation->passes())
        {
            $filename = uniqid('locations_',false).'_'.time().'.'.$request->file('csv')->getClientOriginalExtension();
            $request->file('csv')->move(
                base_path() . '/public/upload/locations/', $filename
                );
            if (($handle = fopen(public_path(). '/upload/locations/'.$filename,'r')) !== FALSE)
            {
                $locs=array();
                while (($data = fgetcsv($handle, 500, ';')) !==FALSE)
                {
                    if(isset($data[0]) && isset($data[1])) {
                        $checklocation = Location::where('address', $data[1])->first();
                        if($checklocation){
                            $locs[] = $checklocation->id;
                        } else {
                            $location = new Location();
                            $location->name = $data[0];
                            $location->address = $data[1];
                            if(isset($data[2])) $location->tags = $data[2];
                            if(isset($data[3]) && preg_match("/^-?([1-8]?[1-9]|[1-9]0)\.{1}\d{1,6}$/", $data[3]) && isset($data[4]) && preg_match("/^-?([1]?[1-7][1-9]|[1]?[1-8][0]|[1-9]?[0-9])\.{1}\d{1,6}$/", $data[4])  ) {
                                $location->latitude = $data[3];
                                $location->longitude = $data[4];
                                $location->is_active = true;
                            } else {
                                $location->is_active = false;
                                $address = str_replace(" ", "+", $data[1]);
                                $latitude = '0.00000000';
                                $longitude = '0.00000000';
                                $region = "Greece";
                                $url = "https://maps.google.com/maps/api/geocode/json?key=".env('GOOGLE_API_KEY')."&address=$address&sensor=false&region=$region";
                                $ch = curl_init();
                                curl_setopt($ch, CURLOPT_URL, $url);
                                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                                curl_setopt($ch, CURLOPT_PROXYPORT, 3128);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
                                $response = curl_exec($ch);
                                curl_close($ch);
                                $response_a = json_decode($response);
                                if(isset($response_a->results[0])) {
                                    $latitude = $response_a->results[0]->geometry->location->lat;
                                    $longitude = $response_a->results[0]->geometry->location->lng;
                                    $location->is_active = true;
                                }
                                $location->latitude = $latitude;
                                $location->longitude = $longitude;
                            }
                            $checklocationdata = Location::where('latitude', $latitude)->where('longitude', $longitude)->first();
                            if($checklocationdata && $latitude != '0.00000000' && $longitude != '0.00000000' ) {
                                $locs[] = $checklocationdata->id;
                            } else {
                                $location->save();
                                $locs[] = $location->id;
                            }
                        }
                    }
                }
                fclose($handle);
            }
            return Redirect::action('LocationsController@results', array('locations' => json_encode($locs)));
            return redirect('admin/locations/results')->with('locations', $locs);
        }
        return Redirect::route('admin.locations.index')
        ->withInput()
        ->withErrors($validation);
    }

    public function results($locs)
    {
        $locs = json_decode($locs);
        $locations = Location::whereIn('id', $locs)->get();
        return View::make('admin.locations.results', compact('locations'));
    }
}
