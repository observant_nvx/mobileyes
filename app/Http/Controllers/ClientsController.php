<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Client;
use App\Taskgroup;
use View;
use Input;
use Validator;
use Redirect;

class ClientsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = Client::with('taskgroups')->get();
        return View::make('admin.clients.index', compact('clients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $client = new Client;
        return View::make('admin.clients.create', compact('client'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Input::all();
        $input['is_active'] = isset($input['is_active']) && $input['is_active'] == 'on' ? true : false;
        $validation = Validator::make($input, Client::$rules);
        if ($validation->passes())
        {
            Client::create($input);
            return Redirect::route('admin.clients.index');
        }
        return Redirect::route('admin.clients.create')
        ->withInput()
        ->withErrors($validation);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $client = Client::with('taskgroups')->findOrFail($id);
        return View::make('admin.clients.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $client = Client::findOrFail($id);
        $input = Input::all();
        $input['is_active'] = isset($input['is_active']) && $input['is_active'] == 'on' ? true : false;
        $validation = Validator::make($input, Client::$rules);
        if ($validation->passes())
        {
            $client->update($input);
            return Redirect::route('admin.clients.index');
        }
        return Redirect::route('admin.clients.edit', [$registration])
        ->withInput()
        ->withErrors($validation);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $client = Client::find($id);
        $checktg = Taskgroup::where('client_id', $id)->first();
        if(!$checktg) {
            $client->delete();
            alert()->success('Client deleted successfully');
        } else {
            $client->update(array('is_active' => false));
            alert()->warning('Client could not be deleted', 'Made inactive instead');
        }
        return Redirect::route('admin.clients.index');
    }
}
