<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\GlobalVar;
use View;
use Input;
use Validator;
use Redirect;

class GlobalVarsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $globalvars = GlobalVar::get()->lists('value', 'key');
        return View::make('admin.globalvars.index', compact('globalvars'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Input::all();
        $validation = Validator::make($input, GlobalVar::$rules);
        if ($validation->passes())
        {
            unset($input['_token']);
            foreach ($input as $key => $value) {
               $globalvar = GlobalVar::where('key', $key)->first();
               $globalvar->update(array('value'=>$value));
            }
            return Redirect::route('admin.globalvars.index');
        }
        return Redirect::route('admin.globalvars.index', [$input])
        ->withInput()
        ->withErrors($validation);
    }
}
