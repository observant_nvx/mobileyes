<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\User;
use App\Profile;
use App\Occupation;
use App\Task;
use DB;
use View;
use Input;
use Carbon;
use Validator;
use Redirect;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::select('users.*')->with('profile')->with('registrations')->with('answers')->get();
        $from = date('Y-01-01');
        $to = date('Y-m-d');
        foreach ($users as $user) {
            
            $user->all_registrations = $user->registrations->count();
            $user->all_registrations_year = $user->registrations->filter(function ($item) use($from,$to) {
                return $item['created_at'] >= $from  &&  $item['created_at'] <= $to;
            })->count();
            $user->pending_registrations = $user->registrations->whereIn('registration_status_id', [1,6,7])->count();
            $user->pending_registrations_year = $user->registrations->whereIn('registration_status_id', [1,6,7])->filter(function ($item) use($from,$to) {
                return $item['created_at'] >= $from  &&  $item['created_at'] <= $to;
            })->count();
            $user->pending_registrations = $user->registrations->whereIn('registration_status_id', [1,6,7])->count();
            $user->completed_registrations = $user->registrations->whereIn('registration_status_id', [11,16])->count();
            $user->completed_registrations_year = $user->registrations->whereIn('registration_status_id', [11,16])->filter(function ($item) use($from,$to) {
                return $item['created_at'] >= $from  &&  $item['created_at'] <= $to;
            })->count();
            $user->failed_registrations = $user->registrations->whereIn('registration_status_id', [21,22])->count();
            $user->failed_registrations_year = $user->registrations->whereIn('registration_status_id', [21,22])->filter(function ($item) use($from,$to) {
                return $item['created_at'] >= $from  &&  $item['created_at'] <= $to;
            })->count();
            $user->all_money = Task::whereIn('id',$user->answers->pluck('task_id'))->sum('money');
            $user->all_money_year = Task::whereIn('id', $user->answers->filter(function ($item) use ($from,$to) {
                return $item['created_at'] >= $from  &&  $item['created_at'] <= $to;
            })->pluck('task_id'))->sum('money');
            $user->pending_money = Task::whereIn('id',$user->answers->whereIn('answer_status_id', [1,6])->pluck('task_id'))->sum('money');
            $user->accepted_money = Task::whereIn('id',$user->answers->where('answer_status_id', 11)->pluck('task_id'))->sum('money');
            // $user->accepted_money_year = Task::whereIn('id', $user->answers->where('answer_status_id', 11)->filter(function ($item) use($from,$to) {
            //     return $item['created_at'] >= $from  &&  $item['created_at'] <= $to;
            // })->pluck('task_id'))->sum('money');
            $user->processed_money = Task::whereIn('id',$user->answers->where('answer_status_id', 12)->pluck('task_id'))->sum('money');
            // $user->processed_money_year = Task::whereIn('id', $user->answers->where('answer_status_id', 12)->filter(function ($item) use($from,$to) {
            //     return $item['created_at'] >= $from  &&  $item['created_at'] <= $to;
            // })->pluck('task_id'))->sum('money');
            $user->paid_money = Task::whereIn('id',$user->answers->where('answer_status_id', 13)->pluck('task_id'))->sum('money');
            $user->paid_money_year = Task::whereIn('id', $user->answers->where('answer_status_id', 13)->filter(function ($item) use($from,$to) {
                return $item['created_at'] >= $from  &&  $item['created_at'] <= $to;
            })->pluck('task_id'))->sum('money');
            $user->not_accepted = Task::whereIn('id',$user->answers->where('answer_status_id', 21)->pluck('task_id'))->sum('money');
            // $user->not_accepted_year = Task::whereIn('id', $user->answers->where('answer_status_id', 21)->filter(function ($item) use($from,$to) {
            //     return $item['created_at'] >= $from  &&  $item['created_at'] <= $to;
            // })->pluck('task_id'))->sum('money');
            //$user->pending_money = Task::getMoney();
            
        }
        return View::make('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = new User;
        $user['occupations'] = Occupation::lists('name','id');
        $user['banks'] = ['' => 'Please Select'] + DB::table('banks')->lists('name','id');
        $user['education'] = ['' => 'Please Select'] + DB::table('education')->lists('name','id');
        $user['user_types'] = DB::table('user_types')->lists('type','id');
        return View::make('admin.users.create', compact('user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Input::all();
        $input['is_active'] = isset($input['is_active']) && $input['is_active'] == 'on' ? true : false;
        $profile = $input['profile']; unset($input['profile']);

        $validateUser = Validator::make($input, User::$rules);
        $validateProfile = Validator::make($profile, Profile::$rules);

        if ($validateUser->fails() OR $validateProfile->fails()) :
            $profilefails = array_combine(array_map(function($field) {
                return 'profile.'.$field;
            }, array_keys($validateProfile->messages()->toArray())), array_values($validateProfile->messages()->toArray()));
        $validationMessages = array_merge_recursive($validateUser->messages()->toArray(), $profilefails);
        else :
            $input['password'] = bcrypt($input['password']);
        unset($input['password_confirmation']);
        $user = new User($input);
        $user->save();
        $user_profile = new Profile($profile);
        $user->profile()->save($user_profile);
        return Redirect::route('admin.users.index');
        endif;
        return Redirect::route('admin.users.create')->withErrors($validationMessages)->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::with('profile')->findOrFail($id);
        $user['occupations'] = Occupation::lists('name','id');
        $user['banks'] = ['' => 'Please Select'] + DB::table('banks')->lists('name','id');
        $user['education'] = ['' => 'Please Select'] + DB::table('education')->lists('name','id');
        $user['user_types'] = DB::table('user_types')->lists('type','id');
        return View::make('admin.users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::findOrFail($id);

        $input = Input::all();
        $input['is_active'] = isset($input['is_active']) && $input['is_active'] == 'on' ? true : false;
        $profile = $input['profile']; unset($input['profile']);

        if($input['password'] == '' && $input['password_confirmation'] == ''){
            $input['password'] = $user->password; 
            $input['password_confirmation'] = $user->password; 
        }

        $validateUser = Validator::make($input, User::$rules);
        $validateProfile = Validator::make($profile, Profile::$rules);
        //dd($validateProfile->messages()->toArray());
        if ($validateUser->fails() OR $validateProfile->fails()) :
            $profilefails = array_combine(array_map(function($field) {
                return 'profile.'.$field;
            }, array_keys($validateProfile->messages()->toArray())), array_values($validateProfile->messages()->toArray()));
        $validationMessages = array_merge_recursive($validateUser->messages()->toArray(), $profilefails);
        else :
            if($input['password'] != $user->password) $input['password'] = bcrypt($input['password']);
        unset($input['password_confirmation']);
        $user->update($input);
        $user->profile()->update($profile);
        return Redirect::route('admin.users.index');
        endif;
        return Redirect::route('admin.users.edit', [$user])->withErrors($validationMessages)->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->update(array('is_active' => false));
        return Redirect::route('admin.users.index');
    }
}
