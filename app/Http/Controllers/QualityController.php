<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Registration;
use App\Task;
use App\Taskgroup;
use App\Answer;
use View;
use DB;
use Input;
use Validator;
use Redirect;

class QualityController extends Controller
{

	/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $taskgroups = Taskgroup::lists('name','id')->prepend('Please select', '');
        return View::make('admin.quality.index', compact('taskgroups'));
    }


	public function show($id, $status=null)
	{
		$statuses = DB::table('answer_statuses')->whereIn('id', [6,11,21])->get();
		$status_ids = $status ? DB::table('answer_statuses')->where('status', $status)->pluck('id') : [6,11,21];
		$registrations = Registration::forQualityProccess($status_ids)->with('taskgroup')->with('location')->with('user')->where('taskgroup_id', $id)->get();
		foreach ($registrations as $registration) {
			$answer_statuses = explode(',', $registration['answer_statuses']);
			$statuses_array = array();
			foreach (array_count_values($answer_statuses) as $key => $value) {
				$answer_status = DB::table('answer_statuses')->where('id', $key)->value('status');
				$statuses_array[$answer_status] = $value;
			}
			$registration->answer_statuses = $statuses_array;
		}
		return View::make('admin.quality.show', compact('id', 'registrations', 'status', 'statuses'));
	}

	public function edit($id, $status=null)
	{
		$status_ids = $status ? DB::table('answer_statuses')->where('status', $status)->pluck('id') : [6,11,21];
		$registration = Registration::with(['answers' => function ($query) use ($status_ids) {
			$query->select('answers.*')->join('tasks', 'answers.task_id', '=', 'tasks.id')->whereIn('answer_status_id', $status_ids)->orderBy('tasks.order', 'asc');
		}])->with('taskgroup')->with('location')->with('user')->with('profile')->findOrFail($id);
		foreach ($registration->answers as $answer) {
			$task = Task::where('id', $answer->task_id)->first();
			$answer->tasktype_id = $task['tasktype_id'];
			$answer->order = $task['order'];
			$answer->money = $task['money'];
			$task['description'] = json_decode($task['description']);
			if(isset($task['description']->answers)) {
				$tmp = array();
				array_walk($task['description']->answers, function ($one) use (&$tmp){                    
					$tmp[$one->value] = $one->text;
				});
				$task['description']->answers = $tmp;
			}
			$answer->description = $task['description'];
			$answer->answer_status_id = $answer->answer_status_id == 11 ? 1 : 0;
		}
		//$statuses = [''=> 'Please select'] + DB::table('answer_statuses')->whereIn('id', [11,21])->lists('status','id');
		return View::make('admin.quality.edit', compact('registration', 'status'));
	}

	public function update(Request $request, $id, $status=null)
	{
		$input = Input::all();
		$registration = Registration::find($id);
		foreach ($input['answer_id'] as $key => $value) {
			$answer = Answer::findOrFail($key);
			$value = isset($input['answer_status_id']) && isset($input['answer_status_id'][$key]) ? 11 : 21;
			$update = array('answer_status_id'=>$value);
			$answer->update($update);
		}
		return Redirect::action('QualityController@show', [$registration->taskgroup_id, $status]);
	}
}
