<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Taskgroup;
use App\Task;
use App\Answer;
use App\Location;
use App\Client;
use App\Registration;
use View;
use Carbon;
use DB;
use Input;
use Validator;
use Redirect;

class TaskgroupsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $taskgroups = Taskgroup::all();
        $statuses = array(''=>'all', 'draft'=>'draft','pending'=>'pending','live'=>'live','paused'=>'paused','ended'=>'ended','completed'=>'completed');
        return View::make('admin.taskgroups.index', compact('taskgroups','statuses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $taskgroup = new Taskgroup;
        $now = strtotime(date("Y-m-d 00:00"));
        $taskgroup->start_date = date('Y-m-d 00:00', strtotime("+1 day", $now));
        $taskgroup->end_date = date('Y-m-d 00:00', strtotime("+5 days", $now));
        $taskgroup->commission_date = date('Y-m-d 00:00', strtotime("+5 days", $now));
        $taskgroup->delivery_date = date('Y-m-d 00:00', strtotime("+5 days", $now));
        $taskgroup->settlement_date = date('Y-m-d 00:00', strtotime("+5 days", $now));
        $taskgroup['orderby'] = array_combine(range(1,10),range(1,10));
        $taskgroup['statuses'] = array_combine(range(0,1),array('inactive','active'));
        $taskgroup['media_types'] = Taskgroup::mediaTypes();
        $clients = [''=> 'Please select'] + Client::active()->lists('name','id')->toArray();
        $taskgroup['available'] = $request->old('available') ? Location::activeValid($request->old('available'))->lists('name_tags','id') : Location::activeValid()->lists('name_tags','id');
        $taskgroup['locations'] = $request->old('locations') ? Location::activeValid($request->old('locations'))->lists('name_tags','id') : array();
        return View::make('admin.taskgroups.create', compact('taskgroup', 'clients'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Input::all();
        unset($input['available']);
        if(!empty(array_values($input['media_url'])[0]['title'])){
            $media_url = Taskgroup::sortArray($input['media_url'], 'order');
            $input['media_url'] = json_encode(array('media' => $media_url));
        } else {
            unset($input['media_url']);
        }
        //$input['is_active'] = isset($input['is_active']) && $input['is_active'] == 'on' ? 1 : 0;
        $messages = ['between' => 'The :attribute must be between :min - :max.'];
        $validation = Validator::make($input, array_merge(Taskgroup::$rules, array('image'=>'required', 'locations'=>'required|between:1,'.$input['max_users'])), $messages);
        if ($validation->passes())
        {
            $locs = isset($input['locations']) ? $input['locations'] : null; unset($input['locations']);
            $filename = uniqid('taskgroup_',false).'_'.time().'.'.$request->file('image')->getClientOriginalExtension();
            $request->file('image')->move(
                base_path() . '/public/images/taskgroups/', $filename
                );
            $input['image'] = $filename;
            $taskgroup = Taskgroup::create($input);
            $taskgroup->locations()->attach($locs);
            return Redirect::route('admin.taskgroups.index');
        }
        return Redirect::route('admin.taskgroups.create')
        ->withInput()
        ->withErrors($validation);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $taskgroup = Taskgroup::with('active_locations')->findOrFail($id);
        $taskgroup['media_url'] = json_decode($taskgroup['media_url']);
        $taskgroup['locations'] = $taskgroup->active_locations->pluck('id')->toArray();
        $taskgroup['orderby'] = array_combine(range(1,10),range(1,10));
        $taskgroup['statuses'] = array_combine(range(0,1),array('inactive','active'));
        $taskgroup['media_types'] = Taskgroup::mediaTypes();
        $clients = [''=> 'Please select'] + Client::active()->lists('name','id')->toArray();
        $all_locations = Location::activeValid()->lists('name_tags','id')->toArray();
        $taskgroup['available'] = $request->old('available') ? Location::activeValid($request->old('available'))->lists('name_tags','id') : Location::ActiveValidNot($taskgroup['locations'])->lists('name_tags','id');
        $taskgroup['locations'] = $request->old('locations') ? Location::activeValid($request->old('locations'))->lists('name_tags','id') : Location::ActiveValid($taskgroup['locations'])->lists('name_tags','id');
        $status = [];
        if(!$taskgroup['is_active'] && $taskgroup['action_date']<$taskgroup['start_date']) : $status['state'] = 'draft'; $status['action'] = 'activate'; $status['btn'] = 'success';
        elseif($taskgroup['is_active'] && date("Y-m-d h:i:s")<$taskgroup['start_date']) : $status['state'] = 'pending';  $status['action'] = 'draft'; $status['btn'] = 'primary';
        elseif($taskgroup['is_active'] && $taskgroup['start_date']<=date("Y-m-d h:i:s") && date("Y-m-d h:i:s")<=$taskgroup['end_date']) : $status['state'] = 'live';  $status['action'] = 'pause'; $status['btn'] = 'warning'; 
        elseif(!$taskgroup['is_active'] && $taskgroup['start_date']<$taskgroup['action_date'] && $taskgroup['action_date']<$taskgroup['end_date']) : $status['state'] = 'paused'; $status['action'] = 'resume'; $status['btn'] = 'success';
        elseif($taskgroup['is_active'] && $taskgroup['end_date']<date("Y-m-d h:i:s")) : $status['state'] = 'ended';  $status['action'] = 'complete'; $status['btn'] = 'info';
        elseif(!$taskgroup['is_active'] && $taskgroup['end_date']<$taskgroup['action_date']) : $status['state'] = 'completed'; $status['action'] = ''; $status['btn'] = '';
        endif;
        return View::make('admin.taskgroups.edit', compact('taskgroup', 'clients', 'status'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $taskgroup = Taskgroup::with('locations')->findOrFail($id);
        $input = Input::all();
        unset($input['available']);
        if(!empty(array_values($input['media_url'])[0]['title'])){
            $media_url = Taskgroup::sortArray($input['media_url'], 'order');
            $input['media_url'] = json_encode(array('media' => $media_url));
        } else {
            unset($input['media_url']);
        }
        $messages = ['between' => 'The :attribute must be between :min - :max.'];
        $validation = Validator::make($input, array_merge(Taskgroup::$rules, array('locations'=>'required|between:1,'.$input['max_users'])), $messages);
        if ($validation->passes())
        {
            $locations = isset($input['locations']) ? $input['locations'] : null; unset($input['locations']);
            if(isset($input['image'])) {
                if(file_exists(base_path()."images/taskgroups/".$taskgroup->photo)) unlink(base_path()."images/taskgroups/".$taskgroup->photo);
                $filename = uniqid('taskgroup_',false).'_'.time().'.'.$request->file('image')->getClientOriginalExtension();
                $request->file('image')->move(
                    base_path() . '/public/images/taskgroups/', $filename
                    );
                $input['image'] = $filename;
            }
            $locations = array_flip($locations);
            foreach ($locations as $key => $value) {
                $locations[$key] = array('is_active'=>true);
            }
            foreach ($taskgroup->locations as $loc) {
                if (!array_key_exists($loc->id, $locations)) {
                    $checkreg = Registration::where('location_id', $loc->id)->where('taskgroup_id', $taskgroup->id)->first();
                    if(!$checkreg) {
                        DB::table('location_taskgroup')->where('location_id', $loc->id)->where('taskgroup_id', $taskgroup->id)->delete();
                    } else {
                        $locations[$loc->id] = array('is_active'=>false);
                    }
                }
            }
            $taskgroup->update($input);
            $taskgroup->locations()->sync($locations);
            return Redirect::route('admin.taskgroups.index');
        }
        return Redirect::route('admin.taskgroups.edit', [$taskgroup])
        ->withInput()
        ->withErrors($validation);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $taskgroup = Taskgroup::find($id);
        $checkreg = Registration::where('taskgroup_id', $id)->first();
        if(!$checkreg) {
            Task::where('taskgroup_id', $taskgroup->id)->delete();
            DB::table('location_taskgroup')->where('taskgroup_id', $taskgroup->id)->delete();
            if(file_exists(base_path()."images/taskgroups/".$taskgroup->photo)) unlink(base_path()."images/taskgroups/".$taskgroup->photo);
            $taskgroup->delete();
            alert()->success('Taskgroup deleted successfully');
        } else {
            $taskgroup->update(array('is_active' => false));
            alert()->warning('Taskgroup could not be deleted', 'Made inactive instead');
        }
        return Redirect::route('admin.taskgroups.index');
    }


    public function images($id)
    {
        $taskgroup = Taskgroup::with('tasks')->find($id);
        $tasks = $taskgroup->tasks->filter(function ($item) {
            return $item->tasktype_id == 5;
        })->values();
        foreach ($tasks as $task) {
            $task['description'] = json_decode($task['description']);
            $task['answers'] = Answer::where('task_id', $task->id)->whereBetween('answer_status_id',array(10,20))->get();
            //$task['answers']['created_at'] = Carbon::setToStringFormat('d-M-Y H:i');
        }
        //dd($task->answers);
        return View::make('admin.taskgroups.images', compact('taskgroup','tasks'));
    }

    public function locations($id)
    {
        $taskgroup = Taskgroup::with('active_locations')->find($id);
        foreach ($taskgroup->active_locations as $loc) {
            $loc['reg_status'] = Registration::where('location_id', $loc['id'])->value('registration_status_id');
            $loc['reg_user'] = Registration::where('location_id', $loc['id'])->value('user_id');
            $loc['reg_time'] = Registration::where('location_id', $loc['id'])->value('created_at');
        }
        return View::make('admin.taskgroups.locations', compact('taskgroup'));
    }

    public function duplicate()
    {

        $taskgroups = Taskgroup::lists('name','id')->prepend('Please select', '');
        return View::make('admin.taskgroups.duplicate', compact('taskgroups'));
    }


    public function post_duplicate(Request $request)
    {
        $input = Input::all();
        $validation = Validator::make($input, array('taskgroup'=>'required'));
        if ($validation->passes())
        {
            $taskgroup = Taskgroup::with('tasks')->with('active_locations')->find($input['taskgroup'])->toArray();
            $locations = array_map('current', $taskgroup['active_locations']); unset($taskgroup['active_locations']);
            $tasks = $taskgroup['tasks']; unset($taskgroup['tasks']);
            $new_taskgroup = Taskgroup::create($taskgroup);
            $new_id = $new_taskgroup->id;
            array_walk($tasks, function ($task) use ($new_id){
                if($task['is_active']) {
                    $task['taskgroup_id'] = $new_id;
                    unset($task['id'], $task['created_at'], $task['updated_at']);
                    Task::create($task);
                }
            });
            $new_taskgroup->locations()->attach($locations);
            return Redirect::action('TaskgroupsController@index');
        }
        return Redirect::action('TaskgroupsController@duplicate')
        ->withInput()
        ->withErrors($validation);
    }

    public function status($id, Request $request)
    {
        $taskgroup = Taskgroup::find($id);
        $input = Input::all();
        if($input['action'] == 'activate') {
            $update = array('is_active' => true);
            alert()->success('Project activated successfully');
        } elseif($input['action'] == 'draft') {
            $update = array('is_active' => false);
            alert()->info('Project changed to draft');
        } elseif($input['action'] == 'pause') {
            $update = array('is_active' => false, 'action_date' => Carbon::now()->toDateTimeString());
            alert()->warning('Project paused successfully');
        } elseif($input['action'] == 'resume') {
            $update = array('is_active' => true);
            alert()->success('Project resumed successfully');
        } elseif($input['action'] == 'complete') {
            $update = array('is_active' => false, 'action_date' => Carbon::now()->toDateTimeString());
            alert()->success('Project completed successfully');
        }
        $taskgroup->update($update);
        return Redirect::route('admin.taskgroups.edit', $taskgroup->id);
    }
}
