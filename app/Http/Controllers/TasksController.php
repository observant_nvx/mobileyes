<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Task;
use App\Taskgroup;
use View;
use Input;
use Validator;
use Redirect;
use DB;

class TasksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $taskgroups = Taskgroup::lists('name','id')->prepend('Please select', '');
        return View::make('admin.tasks.index', compact('taskgroups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //$id = $request->input('id');
        $task = new Task;
        $task['taskgroup_id'] = intval($request->input('id')) ? $request->input('id') : null;
        $task['taskgroups'] = Taskgroup::lists('name','id')->prepend('Please select', '');
        $task['task_types'] = [''=> 'Please select'] + DB::table('task_types')->whereIn('id', array(1,2,4,5,6,10,11,12))->lists('type','id');
        $task['orderby'] = array_combine(range(1,20),range(1,20));
        $task['statuses'] = array_combine(range(0,1),array('inactive','active'));
        return View::make('admin.tasks.create', compact('task'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = Input::all();
        $input['is_active'] = isset($input['is_active']) && $input['is_active'] == 'on' ? 1 : 0;
        if($input['tasktype_id'] == 1 || $input['tasktype_id'] == 2 || $input['tasktype_id'] == 12) {
            if($input['tasktype_id'] == 12) {
                unset($input['description']['answers']);
                $input['description']['answers'][] = array("value" => "1", "text" => "Ναι", "order" => "1","is_active" => "1" );
                $input['description']['answers'][] = array("value" => "0", "text" => "Όχι", "order" => "2","is_active" => "1" );
            }
            $answers = Task::sortArray($input['description']['answers'], 'order');
            $input['description']['answers'] = $answers;
        } else {
            unset($input['description']['answers']);
        }
        $input['description'] = json_encode($input['description']);
        $validation = Validator::make($input, Task::$rules);
        if ($validation->passes())
        {
            Task::create($input);
            return Redirect::route('admin.tasks.show', [$input['taskgroup_id']]);
        }
        return Redirect::route('admin.tasks.create')
        ->withInput()
        ->withErrors($validation);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $tasks = Task::where('taskgroup_id', $id)->get();
        return View::make('admin.tasks.show', compact('id', 'tasks'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $task = Task::findOrFail($id);
        $task['taskgroups'] = Taskgroup::lists('name','id')->prepend('Please select', '');
        $task['task_types'] = [''=> 'Please select'] + DB::table('task_types')->whereIn('id', array(1,2,4,5,6,10,11,12))->lists('type','id');
        $task['description'] = json_decode($task['description']);
        $task['orderby'] = array_combine(range(1,20),range(1,20));
        $task['statuses'] = array_combine(range(0,1),array('inactive','active'));
        return View::make('admin.tasks.edit', compact('task'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $task = Task::findOrFail($id);
        $input = Input::all();
        $input['is_active'] = isset($input['is_active']) && $input['is_active'] == 'on' ? 1 : 0;
        if($input['tasktype_id'] == 1 || $input['tasktype_id'] == 2 || $input['tasktype_id'] == 12) {
            if($input['tasktype_id'] == 12) {
                unset($input['description']['answers']);
                $input['description']['answers'][] = array("value" => "1", "text" => "Ναι", "order" => "1","is_active" => "1" );
                $input['description']['answers'][] = array("value" => "0", "text" => "Όχι", "order" => "2","is_active" => "1" );
            }
            $answers = Task::sortArray($input['description']['answers'], 'order');
            $input['description']['answers'] = $answers;
        } else {
            unset($input['description']['answers']);
        }
        $input['description'] = json_encode($input['description']);

        $validation = Validator::make($input, Task::$rules);
        
        if ($validation->passes())
        {
            $task->update($input);
            return Redirect::route('admin.tasks.show', [$input['taskgroup_id']]);
        }
        return Redirect::route('admin.tasks.edit', [$taskgroup])
        ->withInput()
        ->withErrors($validation);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $task = Task::find($id);
        $task->update(array('is_active' => false));
        return Redirect::route('admin.tasks.index');
    }
}
