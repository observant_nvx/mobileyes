<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GlobalVar extends Model
{
	/**
     * The attributes that are mass assignable.
     *
     * @var array
     */
	protected $fillable = [
	'value',
	];

	public static $rules = array(
		'max_arrival_time' => 'required',
		'max_completion_time' => 'required',
		'max_extra_time' => 'required',
		'radius' => 'required',
		'terms_link' => 'required',
	);
}
