<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

use Carbon;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
    'email', 'mobile', 'password', 'user_type_id', 'is_active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    'password', 'remember_token',
    ];

    public static $rules = array(
        'email' => 'required|email',
        'mobile' => 'required|min:5',
        'password' => 'required|min:5|confirmed',
        'user_type_id' => 'required',
        );

    public function profile()
    {
        return $this->hasOne('App\Profile');
    }

    public function registrations()
    {
        return $this->hasMany('App\Registration');
    }

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function getFullNameAttribute() {
        return ucfirst($this->profile['name']) . ' ' . ucfirst($this->profile['surname']);
    }

    public function getFullAddressAttribute() {
        $address = $this->profile['street']. ' ' . $this->profile['number'];
        if($this->profile['zip_code']) $address .= ',' . $this->profile['zip_code'];
        if($this->profile['county']) $address .= ',' . $this->profile['county'];
        return $address;
    }

    public function getRegistrationDateAttribute()
    {
        return Carbon::parse($this->attributes['created_at']); 
    }

    public function getStatusTextAttribute()
    {
        return $this->attributes['is_active'] ? 'active' : 'inactive'; 
    }

    public function scopeGetProfiles($query) {
        return $query->select()->join('profiles', 'users.id', '=', 'profiles.user_id');
    }

    public function scopeGetProfile($query, $user_id) {
        return $query->select()->join('profiles', 'users.id', '=', 'profiles.user_id')->where('profiles.user_id',$user_id);
    }
}
