<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
	protected $fillable = [
		'name', 'surname', 'birthdate', 'street', 'number', 'zip_code', 'county', 'IBAN', 'bank_id', 'occupation_id', 'education_id', 'details', 
	];

	public static $rules = array(
		'name' => 'required|min:3',
		'surname' => 'required|min:3',
	);

	public function scopeGetByUser($query, $user_id) {
		return $query->select()->where('user_id', $user_id);
	}

	public function user() {
		return $this->belongsTo('User');
	}
}
