<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use Carbon;

class Taskgroup extends Model
{
	protected $fillable = [
	'name', 'short_description', 'long_description', 'start_date', 'end_date', 'image', 'max_users', 'pin_color', 'media_url', 'client_id', 'is_active', 'commission_date', 'delivery_date', 'settlement_date', 'action_date',
	];


	public static $rules = array(
		'name' => 'required|min:3',
		'short_description' => 'required|min:3',
		'long_description' => 'required|min:3',
		'start_date' => 'required|date',
		'end_date' => 'required|date|after:start_date',
		'client_id' => 'required',
		'image'=>'mimes:jpg,jpeg,png|max:500',
		);

	private $media_types = [
     'image' => 'Image',
     'link' => 'Link',
     'pdf' => 'PDF',
     ];

	public function locations()
    {
        return $this->belongsToMany('App\Location')->withPivot('is_active');
    }

    public function active_locations()
    {
        return $this->belongsToMany('App\Location')->wherePivot('is_active', 1);
    }

    public function tasks()
    {
        return $this->hasMany('App\Task');
    }


    public function clients()
    {
        return $this->belongsTo('App\Client');
    }

    public function scopeMediaTypes() {
        return $this->media_types;
    }

    public function getStatusTextAttribute()
    {
        if(!$this->attributes['is_active'] && $this->attributes['action_date']<$this->attributes['start_date']) : $status = 'draft';
        elseif($this->attributes['is_active'] && date("Y-m-d h:i:s")<$this->attributes['start_date']) : $status = 'pending';  $status = 'draft';
        elseif($this->attributes['is_active'] && $this->attributes['start_date']<=date("Y-m-d h:i:s") && date("Y-m-d h:i:s")<=$this->attributes['end_date']) : $status = 'live';
        elseif(!$this->attributes['is_active'] && $this->attributes['start_date']<$this->attributes['action_date'] && $this->attributes['action_date']<$this->attributes['end_date']) : $status = 'paused';
        elseif($this->attributes['is_active'] && $this->attributes['end_date']<date("Y-m-d h:i:s")) : $status = 'ended';
        elseif(!$this->attributes['is_active'] && $this->attributes['end_date']<$this->attributes['action_date']) : $status = 'completed';
        endif;
        return $status; 
    }

    public function getStartDateParseAttribute()
    {
        return Carbon::parse($this->attributes['start_date']); 
    }

    public function getEndDateParseAttribute()
    {
        return Carbon::parse($this->attributes['end_date']); 
    }

    public function getCommissionDateParseAttribute()
    {
        return Carbon::parse($this->attributes['commission_date']); 
    }

    public function getDeliveryDateParseAttribute()
    {
        return Carbon::parse($this->attributes['delivery_date']); 
    }

    public function getSettlementDateParseAttribute()
    {
        return Carbon::parse($this->attributes['settlement_date']); 
    }

    public static function sortArray($data, $field)
	{
		if(!is_array($field)) $field = array($field);
		usort($data, function($a, $b) use($field) {
			$retval = 0;
			foreach($field as $fieldname) {
				if($retval == 0) $retval = strnatcmp($a[$fieldname],$b[$fieldname]);
			}
			return $retval;
		});
		return $data;
	}
}
