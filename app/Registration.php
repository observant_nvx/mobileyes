<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Registration extends Model
{
	protected $fillable = [
		'registration_status_id',
	];


	public static $rules = array(
		'registration_status_id'=>'required',
		);

	
	public function user()
    {
        return $this->hasOne('App\User', 'id', 'user_id');
    }

    public function location()
    {
        return $this->hasOne('App\Location', 'id', 'location_id');
    }

    public function taskgroup()
    {
        return $this->hasOne('App\Taskgroup', 'id', 'taskgroup_id');
    }

    public function profile()
    {
        return $this->hasOne('App\Profile', 'user_id', 'user_id');
    }

    public function answers()
    {
        return $this->hasMany('App\Answer');
    }

    public function scopeForQualityProccess($query, $status_ids) {
        return $query->select(DB::raw('registrations.*, GROUP_CONCAT(answers.answer_status_id ORDER BY answers.answer_status_id DESC) as answer_statuses'))->leftjoin('answers', 'answers.registration_id', '=', 'registrations.id')->whereBetween('registration_status_id', [10, 20])->whereIn('answer_status_id', $status_ids)->groupBy('answers.registration_id');
    }

    public function scopeForPaymentProccess($query, $status_ids) {
        return $query->select(DB::raw('registrations.*, MIN(answers.answer_status_id) as answers_status, SUM(tasks.money) as answers_money'))->leftjoin('answers', 'answers.registration_id', '=', 'registrations.id')->leftjoin('tasks', 'answers.task_id', '=', 'tasks.id')->whereBetween('registration_status_id', [10, 20])->whereIn('answer_status_id', $status_ids)->groupBy('answers.registration_id');
    }

    public function scopeForPaymentProccessGroup($query, $status_ids) {
        return $query->select(DB::raw('MAX(registrations.id) as id, registrations.user_id, registrations.redeem, GROUP_CONCAT(answers.id) as answers, MIN(answers.answer_status_id) as answers_status, SUM(tasks.money) as answers_money '))->leftjoin('answers', 'answers.registration_id', '=', 'registrations.id')->leftjoin('tasks', 'answers.task_id', '=', 'tasks.id')->whereBetween('registration_status_id', [10, 20])->whereIn('answer_status_id', $status_ids)->groupBy('registrations.user_id')->groupBy('registrations.redeem');
    }

    public function getRedeemStatusAttribute()
    {
        return $this->attributes['redeem'] ? 'Yes' : 'No'; 
    }
}
