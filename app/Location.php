<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Location extends Model
{
    protected $fillable = [
    'name', 'address', 'tags', 'latitude', 'longitude', 'is_active',
    ];

    public static $rules = array(
      'name' => 'required|min:3',
      'address' => 'required|min:3',
      'latitude'=> array('required', 'regex:/^[-]?(([0-8]?[0-9])\.(\d+))|(90(\.0+)?)$/'),
      'longitude'=>array('required', 'regex:/^[-]?((((1[0-7][0-9])|([0-9]?[0-9]))\.(\d+))|180(\.0+)?)$/'),
      'is_active' => 'required|boolean',
      );

    public function getStatusTextAttribute()
    {
        return $this->attributes['is_active'] ? 'active' : 'inactive'; 
    }

    public function scopeActiveValid($query, $locations_ids=null)
    {
        if($locations_ids) {
            return $query->select(DB::raw('id , CONCAT(name," (", address,")"," [", tags,"]") AS name_tags'))->whereIn('id', $locations_ids)->where('is_active', true)->where('latitude','!=','0.00000000')->where('longitude','!=','0.00000000')->orderBy('name', 'asc');
        } else {
            return $query->select(DB::raw('id , CONCAT(name," (", address,")"," [", tags,"]") AS name_tags'))->where('is_active', true)->where('latitude','!=','0.00000000')->where('longitude','!=','0.00000000')->orderBy('name', 'asc');
        }
    }

    public function scopeActiveValidNot($query, $locations_ids)
    {
        return $query->select(DB::raw('id , CONCAT(name," (", address,")"," [", tags,"]") AS name_tags'))->where('is_active', true)->whereNotIn('id', $locations_ids)->where('latitude','!=','0.00000000')->where('longitude','!=','0.00000000')->orderBy('name', 'asc');
    }

    // public function getNameTagsAttribute() {
    //     return $this->name.' ('.ucfirst($this->address).')';
    // }

}
