@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="text-left" id="login-form" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}
                        <div class="login-form-main-message"></div>
                        <div class="main-login-form">
                            <div class="login-group">
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="sr-only">E-Mail Address</label>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="email address">
                                    @if ($errors->has('email'))
                                    <span class="help-block">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="sr-only">Password</label>
                                    <input id="password" type="password" class="form-control" name="password" placeholder="password">
                                    @if ($errors->has('password'))
                                    <span class="help-block">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                <div class="form-group login-group-checkbox">
                                    <input type="checkbox" id="remember" name="remember">
                                    <label for="remember">remember</label>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary pull-right">Login</button>
                        </div>
                    </form>
                </div>
                <div class="panel-footer">
                    <p>forgot your password? <a href="{{ url('/password/reset') }}">click here</a></p>
                    <p>new user? <a href="{{ url('/register') }}">create new account</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
