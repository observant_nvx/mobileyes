@extends('layouts.app')

@section('content')
<div class="main-full-back text-center vcenter height-100pc">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="login-form-header">Join</div>
                <div class="login-form-1">
                    <form class="text-left" id="login-form" role="form" method="POST" action="{{ url('/register') }}">
                        {{ csrf_field() }}
                        <div class="login-form-main-message"></div>
                        <div class="main-login-form">
                            <div class="login-group">
                                <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                                    <label for="name" class="sr-only">name</label>
                                    <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" placeholder="name">
                                    @if ($errors->has('name'))
                                    <span class="help-block">{{ $errors->first('name') }}</span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('surname') ? ' has-error' : '' }}">
                                    <label for="surname" class="sr-only">surname</label>
                                    <input id="surname" type="text" class="form-control" name="surname" value="{{ old('surname') }}" placeholder="surname">
                                    @if ($errors->has('surname'))
                                    <span class="help-block">{{ $errors->first('surname') }}</span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                                    <label for="email" class="sr-only">email</label>
                                    <input type="text" class="form-control" id="email" name="email" placeholder="email address" value="{{ old('email') }}">
                                    @if ($errors->has('email'))
                                    <span class="help-block">{{ $errors->first('email') }}</span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                                    <label for="password" class="sr-only">password</label>
                                    <input type="password" class="form-control" id="password" name="password" placeholder="password">
                                    @if ($errors->has('password'))
                                    <span class="help-block">{{ $errors->first('password') }}</span>
                                    @endif
                                </div>
                                <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                                    <label for="password_confirmation" class="sr-only">confirm password</label>
                                    <input type="password" class="form-control" id="password_confirmation" name="password_confirmation" placeholder="confirm password">
                                    @if ($errors->has('password_confirmation'))
                                    <span class="help-block">{{ $errors->first('password_confirmation') }}</span>
                                    @endif
                                </div>
                            </div>
                            <button type="submit" class="login-button"><i class="fa fa-chevron-right"></i></button>
                        </div>
                        <div class="etc-login-form">
                            <p>already have an account? <a href="{{ url('/login') }}">login here</a></p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection