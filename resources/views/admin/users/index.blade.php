@extends('layouts.app')

@section('content')
<div class="main-full-back text-center vcenter min-height-100pc pt-20">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-left">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<h3 class="panel-title lh-2">All Users
							<span class="pull-right lh-16">{{ link_to_route('admin.users.create', 'Add new user', array(), array('class' => 'btn btn-success dib btn-sm')) }}</span>
						</h3>
					</div>
					<div class="panel-body fs-13">
						<div class="row mb-5">
							<span class="col-md-2">Static Data</span>
							<div class="list-inline col-md-10">
								<li><a class="toggle-vis" data-column="0">ID</a></li>
								<li><a class="toggle-vis" data-column="1">Date</a></li>
								<li><a class="toggle-vis" data-column="2">Full Name</a></li>
								<li><a class="toggle-vis" data-column="3">Email</a></li>
								<li><a class="toggle-vis" data-column="-2">Status</a></li>
								<li><a class="toggle-vis" data-column="-1">Actions</a></li>
							</div>
						</div>
						<div class="row mb-5">
							<span class="col-md-2">Personal Data</span>
							<div class="list-inline col-md-10">
								<li><a class="toggle-vis" data-column="4">Mobile</a></li>
								<li><a class="toggle-vis" data-column="5">Address</a></li>
							</div>
						</div>
						<div class="row mb-5">
							<span class="col-md-2">Activity Data</span>
							<div class="list-inline col-md-10">
								<li><a class="toggle-vis" data-column="6">Booked (BKD)</a></li>
								<li><a class="toggle-vis" data-column="7">Booked (BKD) <small>(y)</small></a></li>
								<li><a class="toggle-vis" data-column="8">Pending (PND)</a></li>
								<li><a class="toggle-vis" data-column="9">Completed (CPL)</a></li>
								<li><a class="toggle-vis" data-column="10">Completed (CPL) <small>(y)</small></a></li>
								<li><a class="toggle-vis" data-column="11">Failed (FLD)</a></li>
								<li><a class="toggle-vis" data-column="12">Failed (FLD) <small>(y)</small></a></li>
							</div>
						</div>
						<div class="row mb-5">
							<span class="col-md-2">Rewards Data</span>
							<div class="list-inline col-md-10">
								<li><a class="toggle-vis" data-column="13">All (ALL)</a></li>
								<li><a class="toggle-vis" data-column="14">All (ALL) <small>(y)</small></a></li>
								<li><a class="toggle-vis" data-column="15">Pending (PND)</a></li>
								<li><a class="toggle-vis" data-column="16">Accepted (ACP)</a></li>
								<li><a class="toggle-vis" data-column="17">Not Accepted (NAC)</a></li>
								<li><a class="toggle-vis" data-column="18">Processed (PRC)</a></li>
								<li><a class="toggle-vis" data-column="19">Paid (PAD)</a></li>
								<li><a class="toggle-vis" data-column="20">Paid (PAD) <small>(y)</small></a></li>
							</div>
						</div>
					</div>
					<div class="panel-body">
						@if ($users->count())
						<table class="table table-striped table-condensed fs-13" id="datatable" style="width:100%;">
							<thead>
								<tr>
									<th>ID</th>
									<th>Date</th>
									<th>Full Name</th>
									<th>Email</th>
									<th>Mobile</th>
									<th>Address</th>
									<th>BKD</th>
									<th>BKD <small>(y)</small></th>
									<th>PND</th>
									<th>CPL</th>
									<th>CPL <small>(y)</small></th>
									<th>FLD</th>
									<th>FLD <small>(y)</small></th>
									<th>ALL</th>
									<th>ALL <small>(y)</small></th>
									<th>PND</th>
									<th>ACP</th>
									<th>NAC</th>
									<th>PRC</th>
									<th>PAD</th>
									<th>PAD <small>(y)</small></th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($users as $user)
								<tr>
									<td>{{ $user->id }}</td>
									<td>{{ $user->registration_date->format('Y-m-d H:i') }}</td>
									<td>{{ $user->full_name }}</td>
									<td>{{ $user->email }}</td>
									<td>{{ $user->mobile }}</td>
									<td>{{ $user->full_address }}</td>
									<td>{{ $user->all_registrations }}</td>
									<td>{{ $user->all_registrations_year }}</td>
									<td>{{ $user->pending_registrations }}</td>
									<td>{{ $user->completed_registrations }}</td>
									<td>{{ $user->completed_registrations_year }}</td>
									<td>{{ $user->failed_registrations }}</td>
									<td>{{ $user->failed_registrations_year }}</td>
									<td>{{ $user->all_money }}</td>
									<td>{{ $user->all_money_year }}</td>
									<td>{{ $user->pending_money }}</td>
									<td>{{ $user->accepted_money }}</td>
									<td>{{ $user->not_accepted_money }}</td>
									<td>{{ $user->processed_money }}</td>
									<td>{{ $user->paid_money }}</td>
									<td>{{ $user->paid_money_year }}</td>
									<td>{{ $user->status_text }}</td>
									<td>{!! Html::decode(link_to_route('admin.users.edit', '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', array($user->id), array('class' => 'btn btn-info dib btn-xs'))) !!}
										@if($user->is_active)
										<span class="dib">|</span>
										{{ Form::open(array('method' => 'delete','id'=>'delete_form', 'route' => array('admin.users.destroy', $user->id), 'class' => 'dib' )) }}                       
										{!! Html::decode(Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-xs delete-btn'))) !!}
										{{ Form::close() }}
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<td></td>
									<td><input id="date_range" type="text" placeholder="Select Range" size="12"></td>
									<th>Full Name</th>
									<th>Email</th>
									<th>Mobile</th>
									<th>Address</th>
									<td><input id="min6" type="text" placeholder="Min" size="6"></br><input id="max6" type="text" placeholder="Max" size="6"></td>
									<td><input id="min7" type="text" placeholder="Min" size="6"></br><input id="max7" type="text" placeholder="Max" size="6"></td>
									<td><input id="min8" type="text" placeholder="Min" size="6"></br><input id="max8" type="text" placeholder="Max" size="6"></td>
									<td><input id="min9" type="text" placeholder="Min" size="6"></br><input id="max9" type="text" placeholder="Max" size="6"></td>
									<td><input id="min10" type="text" placeholder="Min" size="6"></br><input id="max10" type="text" placeholder="Max" size="6"></td>
									<td><input id="min11" type="text" placeholder="Min" size="6"></br><input id="max11" type="text" placeholder="Max" size="6"></td>
									<td><input id="min12" type="text" placeholder="Min" size="6"></br><input id="max12" type="text" placeholder="Max" size="6"></td>
									<td><input id="min13" type="text" placeholder="Min" size="6"></br><input id="max13" type="text" placeholder="Max" size="6"></td>
									<td><input id="min14" type="text" placeholder="Min" size="6"></br><input id="max14" type="text" placeholder="Max" size="6"></td>
									<td><input id="min15" type="text" placeholder="Min" size="6"></br><input id="max15" type="text" placeholder="Max" size="6"></td>
									<td><input id="min16" type="text" placeholder="Min" size="6"></br><input id="max16" type="text" placeholder="Max" size="6"></td>
									<td><input id="min17" type="text" placeholder="Min" size="6"></br><input id="max17" type="text" placeholder="Max" size="6"></td>
									<td><input id="min18" type="text" placeholder="Min" size="6"></br><input id="max18" type="text" placeholder="Max" size="6"></td>
									<td><input id="min19" type="text" placeholder="Min" size="6"></br><input id="max19" type="text" placeholder="Max" size="6"></td>
									<td></td>
									<th>Status</th>
									<td></td>
								</tr>
							</tfoot>
						</table>
						@else
						There are no users
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('styles')
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.24/daterangepicker.css" rel="stylesheet">
@endpush
@push('scripts')
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.24/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.24/daterangepicker.min.js"></script>
<script type="text/javascript">
$.fn.dataTable.ext.search.push(
	function( settings, data, dataIndex ) {
		var min = parseInt( $('#min6').val(), 10 );
		var max = parseInt( $('#max6').val(), 10 );
        var age = parseFloat( data[6] ) || 0; // use data for the age column

        if ( ( isNaN( min ) && isNaN( max ) ) ||
        	( isNaN( min ) && age <= max ) ||
        	( min <= age   && isNaN( max ) ) ||
        	( min <= age   && age <= max ) )
        {
        	return true;
        }
        return false;
    });
$.fn.dataTable.ext.search.push(
	function( settings, data, dataIndex ) {
		var min = parseInt( $('#min7').val(), 10 );
		var max = parseInt( $('#max7').val(), 10 );
        var age = parseFloat( data[7] ) || 0; // use data for the age column

        if ( ( isNaN( min ) && isNaN( max ) ) ||
        	( isNaN( min ) && age <= max ) ||
        	( min <= age   && isNaN( max ) ) ||
        	( min <= age   && age <= max ) )
        {
        	return true;
        }
        return false;
    });
$.fn.dataTable.ext.search.push(
	function( settings, data, dataIndex ) {
		var min = parseInt( $('#min8').val(), 10 );
		var max = parseInt( $('#max8').val(), 10 );
        var age = parseFloat( data[8] ) || 0; // use data for the age column

        if ( ( isNaN( min ) && isNaN( max ) ) ||
        	( isNaN( min ) && age <= max ) ||
        	( min <= age   && isNaN( max ) ) ||
        	( min <= age   && age <= max ) )
        {
        	return true;
        }
        return false;
    });
$.fn.dataTable.ext.search.push(
	function( settings, data, dataIndex ) {
		var min = parseInt( $('#min9').val(), 10 );
		var max = parseInt( $('#max9').val(), 10 );
        var age = parseFloat( data[9] ) || 0; // use data for the age column

        if ( ( isNaN( min ) && isNaN( max ) ) ||
        	( isNaN( min ) && age <= max ) ||
        	( min <= age   && isNaN( max ) ) ||
        	( min <= age   && age <= max ) )
        {
        	return true;
        }
        return false;
    });
$.fn.dataTable.ext.search.push(
	function( settings, data, dataIndex ) {
		var min = parseInt( $('#min10').val(), 10 );
		var max = parseInt( $('#max10').val(), 10 );
        var age = parseFloat( data[10] ) || 0; // use data for the age column

        if ( ( isNaN( min ) && isNaN( max ) ) ||
        	( isNaN( min ) && age <= max ) ||
        	( min <= age   && isNaN( max ) ) ||
        	( min <= age   && age <= max ) )
        {
        	return true;
        }
        return false;
    });
$.fn.dataTable.ext.search.push(
	function( settings, data, dataIndex ) {
		var min = parseInt( $('#min11').val(), 10 );
		var max = parseInt( $('#max11').val(), 10 );
        var age = parseFloat( data[11] ) || 0; // use data for the age column

        if ( ( isNaN( min ) && isNaN( max ) ) ||
        	( isNaN( min ) && age <= max ) ||
        	( min <= age   && isNaN( max ) ) ||
        	( min <= age   && age <= max ) )
        {
        	return true;
        }
        return false;
    });
$.fn.dataTable.ext.search.push(
	function( settings, data, dataIndex ) {
		var min = parseInt( $('#min12').val(), 10 );
		var max = parseInt( $('#max12').val(), 10 );
        var age = parseFloat( data[12] ) || 0; // use data for the age column

        if ( ( isNaN( min ) && isNaN( max ) ) ||
        	( isNaN( min ) && age <= max ) ||
        	( min <= age   && isNaN( max ) ) ||
        	( min <= age   && age <= max ) )
        {
        	return true;
        }
        return false;
    });
$.fn.dataTable.ext.search.push(
	function( settings, data, dataIndex ) {
		var min = parseFloat( $('#min13').val(), 10 );
		var max = parseFloat( $('#max13').val(), 10 );
        var age = parseFloat( data[13] ) || 0; // use data for the age column

        if ( ( isNaN( min ) && isNaN( max ) ) ||
        	( isNaN( min ) && age <= max ) ||
        	( min <= age   && isNaN( max ) ) ||
        	( min <= age   && age <= max ) )
        {
        	return true;
        }
        return false;
    });
$.fn.dataTable.ext.search.push(
	function( settings, data, dataIndex ) {
		var min = parseFloat( $('#min14').val(), 10 );
		var max = parseFloat( $('#max14').val(), 10 );
        var age = parseFloat( data[14] ) || 0; // use data for the age column

        if ( ( isNaN( min ) && isNaN( max ) ) ||
        	( isNaN( min ) && age <= max ) ||
        	( min <= age   && isNaN( max ) ) ||
        	( min <= age   && age <= max ) )
        {
        	return true;
        }
        return false;
    });
$.fn.dataTable.ext.search.push(
	function( settings, data, dataIndex ) {
		var min = parseFloat( $('#min15').val(), 10 );
		var max = parseFloat( $('#max15').val(), 10 );
        var age = parseFloat( data[15] ) || 0; // use data for the age column

        if ( ( isNaN( min ) && isNaN( max ) ) ||
        	( isNaN( min ) && age <= max ) ||
        	( min <= age   && isNaN( max ) ) ||
        	( min <= age   && age <= max ) )
        {
        	return true;
        }
        return false;
    });
$.fn.dataTable.ext.search.push(
	function( settings, data, dataIndex ) {
		var min = parseFloat( $('#min16').val(), 10 );
		var max = parseFloat( $('#max16').val(), 10 );
        var age = parseFloat( data[16] ) || 0; // use data for the age column

        if ( ( isNaN( min ) && isNaN( max ) ) ||
        	( isNaN( min ) && age <= max ) ||
        	( min <= age   && isNaN( max ) ) ||
        	( min <= age   && age <= max ) )
        {
        	return true;
        }
        return false;
    });
$.fn.dataTable.ext.search.push(
	function( settings, data, dataIndex ) {
		var min = parseFloat( $('#min17').val(), 10 );
		var max = parseFloat( $('#max17').val(), 10 );
        var age = parseFloat( data[17] ) || 0; // use data for the age column

        if ( ( isNaN( min ) && isNaN( max ) ) ||
        	( isNaN( min ) && age <= max ) ||
        	( min <= age   && isNaN( max ) ) ||
        	( min <= age   && age <= max ) )
        {
        	return true;
        }
        return false;
    });
$.fn.dataTable.ext.search.push(
	function( settings, data, dataIndex ) {
		var min = parseFloat( $('#min18').val(), 10 );
		var max = parseFloat( $('#max18').val(), 10 );
        var age = parseFloat( data[18] ) || 0; // use data for the age column

        if ( ( isNaN( min ) && isNaN( max ) ) ||
        	( isNaN( min ) && age <= max ) ||
        	( min <= age   && isNaN( max ) ) ||
        	( min <= age   && age <= max ) )
        {
        	return true;
        }
        return false;
    });
$.fn.dataTable.ext.search.push(
	function( settings, data, dataIndex ) {
		var min = parseFloat( $('#min19').val(), 10 );
		var max = parseFloat( $('#max19').val(), 10 );
        var age = parseFloat( data[19] ) || 0; // use data for the age column

        if ( ( isNaN( min ) && isNaN( max ) ) ||
        	( isNaN( min ) && age <= max ) ||
        	( min <= age   && isNaN( max ) ) ||
        	( min <= age   && age <= max ) )
        {
        	return true;
        }
        return false;
    });
$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#datatable tfoot th').each( function () {
    	var title = $(this).text();
    	$(this).html( '<input type="text" placeholder="Search '+title+'" size="12" id="fs"  />' );
    } );

    // DataTable
    var table = $('#datatable').DataTable({
    	dom: "<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>>" +
    	"<'row'<'col-sm-12'tr>>" +
    	"<'row'<'col-sm-5'i><'col-sm-7'p>>",
    	buttons: [
    	{
    		extend: "csv",
    		exportOptions: {
    			columns: [0, 1, 2, 3, 4, 5]
    		}
    	},
    	{
    		extend: "excel",
    		exportOptions: {
    			columns: [0, 1, 2, 3, 4, 5]
    		}
    	},
    	{
    		extend: "pdf",
    		exportOptions: {
    			columns: [0, 1, 2, 3, 4, 5]
    		}
    	},
    	{
    		extend: "print",
    		exportOptions: {
    			columns: [0, 1, 2, 3, 4, 5]
    		}
    	}
    	],
    	columnDefs: [{orderable: false, targets: -1 }],
    	order: [[ 0, "desc" ]],
    	stateSave: true
    });
    $('#min6, #max6, #min7, #max8, #min8, #max8, #min9, #max9, #min10, #max10, #min11, #max11, #min12, #max12, #min13, #max13, #min14, #max14, #min15, #max15, #min16, #max16, #min17, #max17, #min18, #max18, #min19, #max19').keyup( function() {
    	table.draw();
    } );
    // Apply the search
    table.columns().every( function () {
    	var that = this;

    	$( 'input#fs', this.footer() ).on( 'keyup change', function () {
    		if ( that.search() !== this.value ) {
    			that
    			.search( this.value )
    			.draw();
    		}
    	} );
    });
    for (var i = 4; i < 21; i++) {
    	table.column( i ).visible( false );
    }

    $('a.toggle-vis').on( 'click', function (e) {
    	e.preventDefault();
        // Get the column API object
        var column = table.column( $(this).attr('data-column') );
        // Toggle the visibility
        column.visible( ! column.visible() );
    });

    // Date range script - Start of the sscript
    $("#date_range").daterangepicker({
    	autoUpdateInput: false,
    	locale: {
    		"cancelLabel": "Clear",
    	}
    });

    $("#date_range").on('apply.daterangepicker', function(ev, picker) {
    	$(this).val(picker.startDate.format('YYYY-MM-DD') + ' to ' + picker.endDate.format('YYYY-MM-DD'));
    	table.draw();
    });

    $("#date_range").on('cancel.daterangepicker', function(ev, picker) {
    	$(this).val('');
    	table.draw();
    });
// Date range script - END of the script

$.fn.dataTableExt.afnFiltering.push(
	function( oSettings, aData, iDataIndex ) {

		var grab_daterange = $("#date_range").val();
		var give_results_daterange = grab_daterange.split(" to ");
		var filterstart = give_results_daterange[0];
		var filterend = give_results_daterange[1];
	    var iStartDateCol = 1; //using column 2 in this instance
	    var iEndDateCol = 1;
	    var tabledatestart = aData[iStartDateCol];
	    var tabledateend= aData[iEndDateCol];

	    if ( !filterstart && !filterend )
	    {
	    	return true;
	    }
	    else if ((moment(filterstart).isSame(tabledatestart) || moment(filterstart).isBefore(tabledatestart)) && filterend === "")
	    {
	    	return true;
	    }
	    else if ((moment(filterstart).isSame(tabledatestart) || moment(filterstart).isAfter(tabledatestart)) && filterstart === "")
	    {
	    	return true;
	    }
	    else if ((moment(filterstart).isSame(tabledatestart) || moment(filterstart).isBefore(tabledatestart)) && (moment(filterend).isSame(tabledateend) || moment(filterend).isAfter(tabledateend)))
	    {
	    	return true;
	    }
	    return false;
	}
	);
});
$('button.delete-btn').on('click', function(e){
	e.preventDefault();
	var self = $(this);
	swal({
		title             : "Are you sure?",
		text              : "This User will change to inactive!",
		type              : "warning",
		showCancelButton  : true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText : "Yes, make inactive!",
		cancelButtonText  : "No, Cancel",
		closeOnConfirm    : false,
		closeOnCancel     : false
	},
	function(isConfirm){
		if(isConfirm){
			self.parents("#delete_form").submit();
		}
		else{
			swal("Cancelled","Your user is safe", "error");
		}
	});
});
</script>
@endpush
