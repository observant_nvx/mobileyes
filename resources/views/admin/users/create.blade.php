@extends('layouts.app')

@section('content')
<div class="main-full-back text-center vcenter min-height-100pc pt-20">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-left">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<h3 class="panel-title lh-2">Create User
						<span class="pull-right">{{ link_to_route('admin.users.index', 'Cancel', array(), array('class' => 'btn btn-danger dib btn-sm')) }}</span>
						</h3>
					</div>
					<div class="panel-body">
						@include('admin.partials._formUser')
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('styles')
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.min.css" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap2/bootstrap-switch.min.css" rel="stylesheet">
@endpush
@push('scripts')
<script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/wysihtml/0.4.15/wysihtml5x-toolbar.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/handlebars.js/1.3.0/handlebars.runtime.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js"></script>
<script type="text/javascript">
$('.switch').bootstrapSwitch();
</script>
<script>
$(".textarea").each(function(){$(this).wysihtml5();});
    $(function () {
        $("#birthdate").datepicker({
        	dateFormat: 'yy-mm-dd',
            changeMonth: true,
            changeYear: true,
            yearRange: '-80:+0'
        });
    });
</script>
@endpush

