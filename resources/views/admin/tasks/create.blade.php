@extends('layouts.app')

@section('content')
<div class="main-full-back text-center vcenter min-height-100pc pt-20">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-left">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<h3 class="panel-title lh-2">Create Job
                        <span class="pull-right">{{ link_to_route('admin.tasks.index', 'Cancel', array(), array('class' => 'btn btn-danger dib btn-sm')) }}</span>
                        </h3>
					</div>
					<div class="panel-body">
						@include('admin.partials._formTask')
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('styles')
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap2/bootstrap-switch.min.css" rel="stylesheet">
@endpush
@push('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js"></script>
<script type="text/javascript">
$('.switch').bootstrapSwitch();
$('#answers_wrap').hide();
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        var rand = Math.floor(Math.random() * 10000000000);
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="row mb-10">'+
                '<div class="col-md-2"><input autocomplete="off" required="1" class="form-control" name="description[answers]['+rand+'][value]" placeholder="Answer value" type="text"/></div>'+
                '<div class="col-md-6"><input autocomplete="off" required="1" class="form-control" name="description[answers]['+rand+'][text]" type="text" placeholder="Answer Order" /></div>'+
                '<div class="col-md-2"><select autocomplete="off" required="1" class="form-control" id="order'+rand+'" name="description[answers]['+rand+'][order]"></select></div>'+
                '<div class="col-md-2"><input autocomplete="off" required="1" name="description[answers]['+rand+'][is_active]" type="hidden" value="1" /><a href="#" class="remove_field input-group-addon">Remove</a></div>');
            //add input box
            var select = document.getElementById('order'+rand);
            nooption = document.createElement('option'); nooption.innerHTML = 'Answer Order'; select.appendChild(nooption);
            for (i = 1; i < 10; i++) {
                var opt = document.createElement('option');
                opt.value = i;
                opt.innerHTML = i;
                select.appendChild(opt);
            }
        }
    });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').parent('div').remove(); x--;
    })
});
$('#tasktype_id').change(function(){
   selection = $(this).val();
   switch(selection)
   {
    case '1':
    $('#answers_wrap').show();
    $("#answers_wrap input").attr("required", "true");
    break;
    case '2':
    $('#answers_wrap').show();
    $("#answers_wrap input").attr("required", "true");
    break;
    default:
    $('#answers_wrap').hide();
    $('#answers_wrap input').removeAttr('required');
    }
});
</script>
@endpush

