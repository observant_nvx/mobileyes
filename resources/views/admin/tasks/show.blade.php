@extends('layouts.app')

@section('content')
<div class="main-full-back text-center vcenter min-height-100pc pt-20">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-left">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<h3 class="panel-title lh-2 pull-left">@if ($tasks->count()) {{ Helper::getTaskGroup($tasks[0]->taskgroup_id) }} @else No jobs found @endif</h3>
						<span class="pull-right">{{ link_to_route('admin.tasks.create', 'Add new job', array('id'=>$id), array('class' => 'btn btn-success dib btn-sm')) }}</span>
						<span class="pull-right mr-10">{{ link_to_route('admin.tasks.index', 'Cancel', array(), array('class' => 'btn btn-danger dib btn-sm')) }}</span>
					</div>

					<div class="panel-body">
						@if ($tasks->count())
						<table class="table table-striped table-condensed fs-13" id="datatable">
							<thead>
								<tr>
									<th>ID</th>
									<th>Question</th>
									<th>Type</th>
									<th>Money</th>
									<th>Order</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($tasks as $task)
								<tr>
									<td>{{ $task->id }}</td>
									<td>{{ Helper::getTaskQuestion($task->id) }}</td>
									<td>{{ Helper::getTaskType($task->tasktype_id) }}</td>
									<td>{{ $task->money }}</td>
									<td>{{ $task->order }}</td>
									<td>{{ $task->status_text }}</td>
									<td nowrap>{!! Html::decode(link_to_route('admin.tasks.edit', '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', array($task->id), array('class' => 'btn btn-info dib btn-sm'))) !!}
										@if($task->is_active)
										<span class="dib">|</span>
										{{ Form::open(array('method' => 'delete','id'=>'delete_form', 'route' => array('admin.tasks.destroy', $task->id), 'class' => 'dib' )) }}                       
										{!! Html::decode(Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm delete-btn'))) !!}
										{{ Form::close() }}
										@endif
									</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						@else
						There are no tasks
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('styles')
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
@endpush
@push('scripts')
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script type="text/javascript">
$('#datatable').DataTable({
	columnDefs: [{orderable: false, targets: -1 }],
	order: [[ 0, "desc" ]],
	stateSave: true
});
$('button.delete-btn').on('click', function(e){
	e.preventDefault();
	var self = $(this);
	swal({
		title             : "Are you sure?",
		text              : "This job will be deleted!",
		type              : "warning",
		showCancelButton  : true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText : "Yes, make inactive!",
		cancelButtonText  : "No, Cancel",
		closeOnConfirm    : false,
		closeOnCancel     : false
	},
	function(isConfirm){
		if(isConfirm){
			self.parents("#delete_form").submit();
		}
		else{
			swal("Cancelled","Your job is safe", "error");
		}
	});
});
</script>
@endpush
