@extends('layouts.app')

@section('content')
<div class="main-full-back text-center vcenter min-height-100pc pt-20">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-left">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<h3 class="panel-title lh-2">Select Project
						<span class="pull-right lh-16">{{ link_to_route('admin.tasks.create', 'Add new job', array(), array('class' => 'btn btn-success dib btn-sm')) }}</span>
						</h3>
					</div>
					<div class="panel-body form-group">
						{{Form::select('taskgroup', $taskgroups, null, ['required' => true, 'autocomplete' => 'off', 'class' => 'form-control']) }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$('.form-control').on('change', function() {
	window.location.href = '/admin/tasks/'+ $(this).find(":selected").val();
});
</script>
@endpush
