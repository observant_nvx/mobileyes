@extends('layouts.app')

@section('content')
<div class="main-full-back text-center vcenter min-height-100pc pt-20">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-left">
				<div class="panel panel-default">
					<div class="panel-heading">
						<h3 class="panel-title">Bulk Upload </h3>
					</div>
					<div class="panel-body">
						{{ Form::open(array('method' => 'post','url' => array('admin/locations/bulk'), 'files' => true)) }}
						{{ Form::file('csv', array('accept'=>'.csv')) }}
						{{ Form::submit('Submit', array('class' => 'btn btn-info btn-md mt-20 pull-right')) }}
						{{ Form::close() }}
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<h3 class="panel-title lh-2">All locations
						<span class="pull-right lh-16">{{ link_to_route('admin.locations.create', 'Add new location', array(), array('class' => 'btn btn-success dib btn-sm')) }}</span>
						</h3>
					</div>
					<div class="panel-body">
						@if ($locations->count())
						<table class="table table-striped table-condensed fs-13" id="datatable">
							<thead>
								<tr>
									<th>ID</th>
									<th>Name</th>
									<th>Address</th>
									<th>Tags</th>
									<th>Latitude</th>
									<th>Longitude</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($locations as $location)
								<tr>
									<td>{{ $location->id }}</td>
									<td>{{ $location->name }}</td>
									<td>{{ $location->address }}</td>
									<td>{{ $location->tags }}</td>
									<td>{{ $location->latitude }}</td>
									<td>{{ $location->longitude }}</td>
									<td>{{ $location->status_text }}</td>
									<td nowrap>{!! Html::decode(link_to_route('admin.locations.edit', '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', array($location->id), array('class' => 'btn btn-info dib btn-sm'))) !!}
										<span class="dib">|</span>
										{{ Form::open(array('method' => 'delete','id'=>'delete_form', 'route' => array('admin.locations.destroy', $location->id), 'class' => 'dib' )) }}                       
										{!! Html::decode(Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-sm delete-btn'))) !!}
										{{ Form::close() }}
									</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<td></td>
									<th>Name</th>
									<th>Address</th>
									<th>Tags</th>
									<td></td>
									<td></td>
									<th>Status</th>
									<td></td>
								</tr>
							</tfoot>
						</table>
						@else
						There are no locations
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('styles')
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet">
@endpush
@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-filestyle/1.2.1/bootstrap-filestyle.min.js"></script>
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script type="text/javascript">
$(function () {
	$(":file").filestyle();
});
$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#datatable tfoot th').each( function () {
    	var title = $(this).text();
    	$(this).html( '<input type="text" placeholder="Search '+title+'" size="12" id="fs"  />' );
    } );

    // DataTable
    var table = $('#datatable').DataTable({
    	dom: "<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>>" +
    	"<'row'<'col-sm-12'tr>>" +
    	"<'row'<'col-sm-5'i><'col-sm-7'p>>",
    	buttons: [
    	{
    		extend: "csv",
    		exportOptions: {
    			columns: [0, 1, 2, 3, 4]
    		}
    	},
    	{
    		extend: "excel",
    		exportOptions: {
    			columns: [0, 1, 2, 3, 4]
    		}
    	},
    	{
    		extend: "pdf",
    		exportOptions: {
    			columns: [0, 1, 2, 3, 4]
    		}
    	},
    	{
    		extend: "print",
    		exportOptions: {
    			columns: [0, 1, 2, 3, 4]
    		}
    	}
    	],
    	columnDefs: [{orderable: false, targets: -1 }],
    	order: [[ 0, "desc" ]],
    	stateSave: true
    });

    // Apply the search
    table.columns().every( function () {
    	var that = this;

    	$( 'input#fs', this.footer() ).on( 'keyup change', function () {
    		if ( that.search() !== this.value ) {
    			that
    			.search( this.value )
    			.draw();
    		}
    	} );
    });

});
$('button.delete-btn').on('click', function(e){
	e.preventDefault();
	var self = $(this);
	swal({
		title             : "Are you sure?",
		text              : "This location will be deleted!",
		type              : "warning",
		showCancelButton  : true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText : "Yes, deleted it!",
		cancelButtonText  : "No, Cancel",
		closeOnConfirm    : false,
		closeOnCancel     : false
	},
	function(isConfirm){
		if(isConfirm){
			self.parents("#delete_form").submit();
		}
		else{
			swal("Cancelled","Your location is safe", "error");
		}
	});
});
</script>
@endpush
