@extends('layouts.app')

@section('content')
<div class="main-full-back text-center vcenter min-height-100pc pt-20">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-left">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<h3 class="panel-title lh-2">Create Location
						<span class="pull-right">{{ link_to_route('admin.locations.index', 'Cancel', array(), array('class' => 'btn btn-danger dib btn-sm')) }}</span>
						</h3>
					</div>
					<div class="panel-body">
						<div id="my_map" class="mapCanvas mb-20" style="width:100%;height:250px;"></div>
						@include('admin.partials._formLocation')
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap2/bootstrap-switch.min.css" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1/themes/flick/jquery-ui.css">
<link href="https://cdnjs.cloudflare.com/ajax/libs/tag-it/2.0/css/jquery.tagit.min.css" rel="stylesheet">
@endpush
@push('scripts')
<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.24/jquery-ui.min.js'></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={!!env('GOOGLE_API_KEY') !!}&amp;libraries=places&amp;language=en-GB"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tag-it/2.0/js/tag-it.min.js"></script>
<script type="text/javascript">
$('.switch').bootstrapSwitch();
$(function(){
	$('.tagit').tagit();
});
</script>
<script>
var options = 
{
	details: "form ",
	detailsAttribute: "data-geo",
	map: "#my_map",
	markerOptions: {
		draggable: true
	}
};
$("#address").geocomplete(options).bind("geocode:result", function(event, result){
	var map = $("#address").geocomplete("map");
	map.setZoom(14);
	map.setCenter(result.geometry.location);
});
$("#address").bind("geocode:dragged", function(event, latLng){
	$("input[name=latitude]").val(latLng.lat());
	$("input[name=longitude]").val(latLng.lng());
	var map = $("#address").geocomplete("map");
	map.panTo(latLng);
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode({'latLng': latLng }, function(results, status) {
		if (status == google.maps.GeocoderStatus.OK) {
		//console.log(results);
		if (results[1] && results[0]) {
			$("input#address").val(results[0].formatted_address);
		} else {
			alert("No results found");
		}
	} else {
		alert("Geocoder failed due to: " + status);
	}
});
});
$('#address').on('change keydown paste input', function(){
	$('input#latitude').val('');
	$('input#longitude').val('');
});
</script>
@endpush

