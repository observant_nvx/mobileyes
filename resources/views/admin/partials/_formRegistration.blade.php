{!! BootForm::open(['model' => $registration, 'update' => 'RegistrationsController@update']) !!}
{!! BootForm::text('user[email]', 'Email', null, ['disabled'=>'disabled']) !!}
{!! BootForm::text('taskgroup[name]', 'Taskgroup', null, ['disabled' => 'disabled']) !!}
{!! BootForm::text('location[name]', 'Location', null, ['disabled' => 'disabled']) !!}
{!! BootForm::select('registration_status_id', 'Status', $statuses, null) !!}
{!! BootForm::submit('Submit') !!}
{!! BootForm::close() !!}