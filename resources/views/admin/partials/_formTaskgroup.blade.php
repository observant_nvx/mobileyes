{!! BootForm::open(['model' => $taskgroup, 'store' => 'TaskgroupsController@store', 'update' => 'TaskgroupsController@update', 'enctype' => "multipart/form-data"]) !!}
{!! BootForm::text('name', 'Name', null, ['required' => true]) !!}
{!! BootForm::textarea('short_description', 'Short Description', null, ['class'=>'textarea', 'id'=> 'short', 'rows'=> 5]) !!}
{!! BootForm::textarea('long_description', 'Long Description', null, ['class'=>'textarea', 'id'=> 'long']) !!}
{!! BootForm::text('pin_color', 'Color', null, ['class'=>'jscolor {hash:true}','required' => true]) !!}
<div class="form-group{{ $errors->has('start_date') ? ' has-error' : '' }}">
	<label for="start_date" class="control-label">Start Date</label>
	<div class='input-group date' id="start_date">
		<input name="start_date" type='text' class="form-control" required />
		<span class="input-group-addon">
			<span class="glyphicon glyphicon-calendar"></span>
		</span>
	</div>
	@if ($errors->has('start_date'))
	<span class="help-block">{{ $errors->first('start_date') }}</span>
	@endif
</div>
<div class="form-group{{ $errors->has('end_date') ? ' has-error' : '' }}">
	<label for="end_date" class="control-label">End Date</label>
	<div class='input-group date' id="end_date">
		<input name="end_date" type='text' class="form-control" required/>
		<span class="input-group-addon">
			<span class="glyphicon glyphicon-calendar"></span>
		</span>
	</div>
	@if ($errors->has('end_date'))
	<span class="help-block">{{ $errors->first('end_date') }}</span>
	@endif
</div>
<div class="form-group input_fields_wrap" id="media_wrap">
	<label class="control-label">Media Files </label>
	<button class="add_field_button btn pull-right mb-10">Add More Media</button>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-3">
			<label class="control-label">Title</label>
		</div>
		<div class="col-md-3">
			<label class="control-label">URL</label>
		</div>
		<div class="col-md-2">
			<label class="control-label">Type</label>
		</div>
		<div class="col-md-2">
			<label class="control-label">Order</label>
		</div>
		<div class="col-md-2">
			<label class="control-label">Active</label>
		</div>
	</div>
	@if ($taskgroup->media_url)
	@foreach ($taskgroup['media_url']->media as $media)
	@php $rand = mt_rand();@endphp
	<div class="row mb-10">
		<div class="col-md-3">
			{{Form::text('media_url['.$rand.'][title]', $media->title, ['required' => true, 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Media title']) }}
		</div>
		<div class="col-md-3">
			{{Form::text('media_url['.$rand.'][url]', $media->url, ['required' => true, 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Media URL']) }}
		</div>
		<div class="col-md-2">
			{{Form::select('media_url['.$rand.'][type]', $taskgroup['media_types'], $media->type, ['required' => true, 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Media Type']) }}
		</div>
		<div class="col-md-2">
			{{Form::select('media_url['.$rand.'][order]', $taskgroup['orderby'], $media->order, ['required' => true, 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Media Order']) }}
		</div>
		<div class="col-md-2">
			{{Form::select('media_url['.$rand.'][is_active]', $taskgroup['statuses'], $media->is_active, ['required' => true, 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Media Status']) }}
		</div>
	</div>
	@endforeach
	@else
	<div class="row mb-10">
		@php $rand = mt_rand();@endphp
		<div class="col-md-3">
			{{Form::text('media_url['.$rand.'][title]', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Media title']) }}
		</div>
		<div class="col-md-3">
			{{Form::text('media_url['.$rand.'][url]', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Media URL']) }}
		</div>
		<div class="col-md-2">
			{{Form::select('media_url['.$rand.'][type]', $taskgroup['media_types'], null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Media Type']) }}
		</div>
		<div class="col-md-2">
			{{Form::select('media_url['.$rand.'][order]', $taskgroup['orderby'], null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Media Order']) }}
		</div>
		<div class="col-md-2">
			{{Form::select('media_url['.$rand.'][is_active]', $taskgroup['statuses'], 1, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Media Status']) }}
		</div>
	</div>
	@endif
</div>
{!! BootForm::file('image', 'Image') !!}
@if ($taskgroup->image) 
<div class="form-group "><div class="thumbnail">{{ Html::image(Helper::getTaskgroupImage($taskgroup->image), $taskgroup->name, array('class' => 'width-80 br-circle text-left')) }}</div></div>
@endif
{!! BootForm::number('max_users', 'Max Users', null) !!}
<!-- <div class="form-group ">
	{!! Form::label('is_active', 'Active', ['class'=> 'control-label']) !!}
	{!! Form::checkbox('is_active', null, null, ['class'=> 'switch']) !!}
</div> -->
<!--{!! BootForm::number('max_completion_time', 'Max Completion Time', null) !!}-->
<!--{!! BootForm::number('radius_of_visibility', 'Radius of Visibility', null) !!}-->
{!! BootForm::select('client_id', 'Client', $clients, null, ['class'=> 'chosen']) !!}
<div class="form-group clearfix{{ $errors->has('available') ? ' has-error' : '' }}">
	{!! Form::label('available', 'Available Locations', ['class'=> 'col-xs-12 pl-0 pr-0']) !!}
	<div class="col-xs-12 pl-0 pr-0">
		{{Form::select('available[]', $taskgroup['available'], null, ['id'=> 'multiselect', 'class'=> 'form-control', 'size'=> 8, 'multiple' => 'multiple']) }}
	</div>
	@if ($errors->has('available'))
	<span class="help-block">{{ $errors->first('available') }}</span>
	@endif
</div>
<div class="form-group clearfix">
	<div class="col-xs-3"><button type="button" id="multiselect_rightSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-down"></i></button></div>
	<div class="col-xs-3"><button type="button" id="multiselect_leftSelected" class="btn btn-block"><i class="glyphicon glyphicon-chevron-up"></i></button></div>
	<div class="col-xs-3"><button type="button" id="multiselect_rightAll" class="btn btn-block"><i class="glyphicon glyphicon-arrow-down"></i></button></div>
	<div class="col-xs-3"><button type="button" id="multiselect_leftAll" class="btn btn-block"><i class="glyphicon glyphicon-arrow-up"></i></button></div>
</div>
<div class="form-group clearfix{{ $errors->has('locations') ? ' has-error' : '' }}">
	{!! Form::label('locations', 'Selected Locations', ['class'=> 'col-xs-12 pl-0 pr-0']) !!}
	<div class="col-xs-12 pl-0 pr-0">
		{{Form::select('locations[]', $taskgroup['locations'], null, ['id'=> 'multiselect_to', 'class'=> 'form-control', 'size'=> 8, 'multiple' => 'multiple']) }}
	</div>
	@if ($errors->has('locations'))
	<span class="help-block">{{ $errors->first('locations') }}</span>
	@endif
</div>
<div class="form-group">
	<label for="commission_date" class="control-label">Commission Date</label>
	<div class='input-group date' id="commission_date">
		<input name="commission_date" type='text' class="form-control" required />
		<span class="input-group-addon">
			<span class="glyphicon glyphicon-calendar"></span>
		</span>
	</div>
</div>
<div class="form-group">
	<label for="delivery_date" class="control-label">Delivery Date</label>
	<div class='input-group date' id="delivery_date">
		<input name="delivery_date" type='text' class="form-control" required/>
		<span class="input-group-addon">
			<span class="glyphicon glyphicon-calendar"></span>
		</span>
	</div>
</div>
<div class="form-group">
	<label for="settlement_date" class="control-label">Settlement Date</label>
	<div class='input-group date' id="settlement_date">
		<input name="settlement_date" type='text' class="form-control" required />
		<span class="input-group-addon">
			<span class="glyphicon glyphicon-calendar"></span>
		</span>
	</div>
</div>
{!! BootForm::submit('Submit') !!}
{!! BootForm::close() !!}