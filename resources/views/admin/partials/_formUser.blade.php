{!! BootForm::open(['model' => $user, 'store' => 'UsersController@store', 'update' => 'UsersController@update']) !!}
{!! BootForm::text('profile[name]', 'First Name', null, ['required' => true]) !!}
{!! BootForm::text('profile[surname]', 'Last Name', null, ['required' => true]) !!}
{!! BootForm::email('email', 'Email', null, ['required' => true]) !!}
{!! BootForm::text('mobile', 'Mobile', null, ['required' => true]) !!}
{!! BootForm::password('password', 'Password') !!}
{!! BootForm::password('password_confirmation', 'Confirm Password') !!}
{!! BootForm::text('profile[birthdate]', 'Birth Date', null, ['id' => 'birthdate' ]) !!}
{!! BootForm::text('profile[street]', 'Address Street', null) !!}
{!! BootForm::text('profile[number]', 'Address Number', null) !!}
{!! BootForm::text('profile[zip_code]', 'Address Zip Code', null) !!}
{!! BootForm::text('profile[county]', 'Address County', null) !!}
{!! BootForm::select('profile[bank_id]', 'Bank', $user['banks']) !!}
{!! BootForm::text('profile[IBAN]', 'IBAN', null) !!}
{!! BootForm::select('profile[occupation_id]', 'Occupation', $user['occupations']) !!}
{!! BootForm::select('profile[education_id]', 'Education', $user['education']) !!}
{!! BootForm::textarea('profile[details]', 'Details', null, ['class' => 'textarea' ]) !!}
{!! BootForm::select('user_type_id', 'User Type', $user['user_types']) !!}
<div class="form-group">
{!! Form::label('is_active', 'Active', ['class'=> 'control-label']) !!}
{!! Form::checkbox('is_active', null, null, ['class'=> 'switch']) !!}
</div>
{!! BootForm::submit('Submit') !!}
{!! BootForm::close() !!}