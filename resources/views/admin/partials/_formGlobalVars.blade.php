{!! BootForm::open(['store' => 'GlobalVarsController@store']) !!}
{!! BootForm::text('max_arrival_time', 'Max Arrival Time', $globalvars['max_arrival_time'], ['required' => true]) !!}
{!! BootForm::text('max_completion_time', 'Max Completion Time', $globalvars['max_completion_time'], ['required' => true]) !!}
{!! BootForm::text('max_extra_time', 'Max Extra Time', $globalvars['max_extra_time'], ['required' => true]) !!}
{!! BootForm::text('max_checkin_distance', 'Max Checkin Distance', $globalvars['max_checkin_distance'], ['required' => true]) !!}
{!! BootForm::text('redeem_limit', 'Redeem Limit', $globalvars['redeem_limit'], ['required' => true]) !!}
{!! BootForm::text('radius', 'Radius', $globalvars['radius'], ['required' => true]) !!}
{!! BootForm::text('terms_link', 'Terms Link', $globalvars['terms_link'], ['required' => true]) !!}
{!! BootForm::submit('Submit') !!}
{!! BootForm::close() !!}