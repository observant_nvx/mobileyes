{!! BootForm::open(['model' => $task, 'store' => 'TasksController@store', 'update' => 'TasksController@update']) !!}
{!! BootForm::select('taskgroup_id', 'Task Group', $task['taskgroups'], null, ['required' => true]) !!}
{!! BootForm::select('tasktype_id', 'Task Type', $task['task_types'], null, ['required' => true]) !!}
<div class="row">
	<div class="col-md-2">
		{!! BootForm::text('description[label]', 'Label', null, ['required' => true]) !!}
	</div>
	<div class="col-md-10">
		{!! BootForm::text('description[question]', 'Question', null, ['required' => true]) !!}
	</div>
</div>
<div class="form-group input_fields_wrap" id="answers_wrap" @php if($task['tasktype_id']!=1 && $task['tasktype_id']!=2) echo 'style="display: none;"';@endphp>
	<label class="control-label">Answers </label>
	<button class="add_field_button btn pull-right mb-10">Add More Fields</button>
	<div class="clearfix"></div>
	<div class="row">
		<div class="col-md-2">
			<label class="control-label">Value</label>
		</div>
		<div class="col-md-6">
			<label class="control-label">Text</label>
		</div>
		<div class="col-md-2">
			<label class="control-label">Order</label>
		</div>
		<div class="col-md-2">
			<label class="control-label">Active</label>
		</div>
	</div>
	@if ($task['tasktype_id'] == 1 || $task['tasktype_id'] == 2)
	@foreach ($task['description']->answers as $answer)
	@php $rand = mt_rand();@endphp
	<div class="row mb-10">
		<div class="col-md-2">
			{{Form::text('description[answers]['.$rand.'][value]', $answer->value, ['required' => true, 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Answer value']) }}
		</div>
		<div class="col-md-6">
			{{Form::text('description[answers]['.$rand.'][text]', $answer->text, ['required' => true, 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Answer text']) }}
		</div>
		<div class="col-md-2">
			{{Form::select('description[answers]['.$rand.'][order]', $task['orderby'], $answer->order, ['required' => true, 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Answer Order']) }}
		</div>
		<div class="col-md-2">
			{{Form::select('description[answers]['.$rand.'][is_active]', $task['statuses'], $answer->is_active, ['required' => true, 'autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Answer Status']) }}
		</div>
	</div>
	@endforeach
	@else
	<div class="row mb-10">
		@php $rand = mt_rand();@endphp
		<div class="col-md-2">
			{{Form::text('description[answers]['.$rand.'][value]', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Answer value']) }}
		</div>
		<div class="col-md-6">
			{{Form::text('description[answers]['.$rand.'][text]', null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Answer text']) }}
		</div>
		<div class="col-md-2">
			{{Form::select('description[answers]['.$rand.'][order]', $task['orderby'], null, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Answer Order']) }}
		</div>
		<div class="col-md-2">
			{{Form::select('description[answers]['.$rand.'][is_active]', $task['statuses'], 1, ['autocomplete' => 'off', 'class' => 'form-control', 'placeholder' => 'Answer Status']) }}
		</div>
	</div>
	@endif
</div>
{!! BootForm::number('money', 'Money', null, ['required' => true,'min' => '0.01','step' => '0.01','max' => '2500']) !!}
{!! BootForm::select('order', 'Order', $task['orderby'], null, ['required' => true]) !!}
<div class="form-group ">
	{!! Form::label('is_active', 'Active', ['class'=> 'control-label']) !!}
	{!! Form::checkbox('is_active', null, null, ['class'=> 'switch']) !!}
</div>
{!! BootForm::submit('Submit') !!}
{!! BootForm::close() !!}