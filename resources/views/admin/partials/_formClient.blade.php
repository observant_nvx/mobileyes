{!! BootForm::open(['model' => $client, 'store' => 'ClientsController@store', 'update' => 'ClientsController@update']) !!}
{!! BootForm::text('name', 'Name', null, ['required' => true]) !!}
{!! BootForm::text('address', 'Address', null) !!}
{!! BootForm::textarea('contact_data', 'Contact Data', null, ['class' => 'textarea' ] ) !!}
{!! BootForm::textarea('payment_data', 'Payment Data', null, ['class' => 'textarea' ]) !!}
{!! BootForm::text('type', 'Type', null, ['class' => 'tagit' ]) !!}
<div class="form-group">
{!! Form::label('is_active', 'Active', ['class'=> 'control-label']) !!}
{!! Form::checkbox('is_active', null, null, ['class'=> 'switch']) !!}
</div>
{!! BootForm::submit('Submit') !!}
{!! BootForm::close() !!}