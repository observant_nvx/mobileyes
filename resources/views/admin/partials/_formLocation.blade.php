{!! BootForm::open(['model' => $location, 'store' => 'LocationsController@store', 'update' => 'LocationsController@update']) !!}
{!! BootForm::text('address', 'Address', null, ['id'=>'address']) !!}
{!! BootForm::text('name', 'Name', null, ['required' => true]) !!}
{!! BootForm::text('latitude', 'Latitude', null, ['readonly' => true,'data-geo'=>'lat']) !!}
{!! BootForm::text('longitude', 'Longitude', null, ['readonly' => true,'data-geo'=>'lng']) !!}
{!! BootForm::text('tags', 'Tags', null, ['class' => 'tagit' ]) !!}
<div class="form-group">
{!! Form::label('is_active', 'Active', ['class'=> 'control-label']) !!}
{!! Form::checkbox('is_active', null, null, ['class'=> 'switch']) !!}
</div>
{!! BootForm::submit('Submit') !!}
{!! BootForm::close() !!}