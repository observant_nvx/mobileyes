@extends('layouts.app')

@section('content')
<div class="main-full-back text-center vcenter min-height-100pc pt-20">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-left">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<h3 class="panel-title lh-16">
							Quality {{Html::linkAction('QualityController@index', 'Cancel', $status, array('class' => 'btn btn-danger dib btn-sm pull-right text-white'))}}
						</h3>
					</div>
					<div class="panel-body">
						<h5 class="mt-0">Project: {{ $registration['taskgroup']->name }}</h5>
						<h5 class="mt-0 mb-10">User: {{ $registration['profile']->name }} {{ $registration['profile']->surname }} ({{ $registration['user']->email }})</h5>
						<h5 class="mt-0 mb-0">Location: {{ $registration['location']->name }}</h5>
					</div>
				</div>
				@if ($registration['answers'])
				{!! BootForm::open(array('action' => array('QualityController@update', $registration->id, $status ))) !!}
				@foreach ($registration['answers'] as $answer)
				<div class="panel panel-default mb-20">
					<div class="panel-heading clearfix">
						<h5 class="mt-0 mb-0 lh-2 col-md-5 pl-0"><small>{{$answer->order}}.</small> @if(isset($answer['description']->label)) {{$answer['description']->label}} @else Question @endif: {{$answer['description']->question}}</h5>
						<span class="btn btn-info btn-sm col-md-2 text-white text-center">Money: {{$answer['money']}}</span>
						<h5 class="mt-0 mb-0 lh-2 col-md-5 pr-0 text-right">Approved
							<span class="text-white ml-10">
								{!! Form::checkbox('answer_status_id['.$answer["id"].']', null, $answer['answer_status_id'], ['class'=> 'switch', 'data-off-icon-cls' => 'gluphicon-thumbs-down', 'data-on-icon-cls' => 'gluphicon-thumbs-up']) !!}
								{!! Form::hidden('answer_id['.$answer["id"].']', null, null) !!}
							</span>
						</h5>
					</div>
					<div class="panel-body">
						@if($answer['tasktype_id']==10 || $answer['tasktype_id']==11)
						{!! BootForm::text(null, 'Answer', $answer['data'], ['readonly' => true, 'rows' => 3, ]) !!}
						@elseif($answer['tasktype_id']==4)
						{!! BootForm::textarea(null, 'Answer', $answer['data'], ['readonly' => true, 'rows' => 3, ]) !!}
						@elseif ($answer['tasktype_id']==5)
						{{ Html::image('images/answers/'.$answer['data'], $answer['description']->question, array('class' => 'width-200 make-center')) }}
						@elseif ($answer['tasktype_id']==6)
						@php $coord = explode(",",substr($answer['data'], 1, -1));@endphp
						{!! BootForm::text(null, 'Latitude', $coord[0], ['readonly' => true ]) !!}
						{!! BootForm::text(null, 'Longnitude', $coord[1], ['readonly' => true ]) !!}
						@elseif ($answer['tasktype_id']==1 || $answer['tasktype_id']==12)
						@php $answer['data'] =  intval($answer['data']); @endphp
						{!! BootForm::radios(null, 'Answers', $answer['description']->answers, $answer['data'], ['disabled' => true ]) !!}
						@elseif ($answer['tasktype_id']==2)
						@php $answer['data'] =  explode(',',$answer['data']); @endphp
						{!! BootForm::checkboxes(null, 'Answers', $answer['description']->answers, $answer['data'], ['disabled' => true ]) !!}
						@endif
					</div>
				</div>
				@endforeach
				<div class="panel panel-default">
					<div class="panel-footer clearfix">
						{!! BootForm::submit('Submit',  ['class' => 'btn btn-primary pull-right text-white pull-right' ]) !!}
					</div>
				</div>
				{{ BootForm::close() }}
				@else
				There are no answers
				@endif
			</div>
		</div>
	</div>
</div>
</div>
@endsection
@push('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap2/bootstrap-switch.min.css" rel="stylesheet">
@endpush
@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={!!env('GOOGLE_API_KEY') !!}&amp;libraries=places&amp;language=en-GB"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-checkbox/1.4.0/bootstrap-checkbox.min.js"></script>
<script type="text/javascript">
$('.switch').checkboxpicker();
</script>
<script type="text/javascript">
$(':radio,input:not(".switch"):checkbox').click(function(){
	return false;
});
</script>
@endpush