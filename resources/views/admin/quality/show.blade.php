@extends('layouts.app')

@section('content')
<div class="main-full-back text-center vcenter min-height-100pc pt-20">
	<div class="container">
		<div class="row mb-20">
			<div class="col-md-10 col-md-offset-1 text-left">
				<ul class="list-inline">
					<li>{!! Html::decode(Html::linkAction('QualityController@show', 'all', [$id])) !!}</li>
					@foreach ($statuses as $statuses)
					<li>{!! Html::decode(Html::linkAction('QualityController@show', $statuses->status, [$id, $statuses->status])) !!}</li>
					@endforeach
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1 text-left">
					<div class="panel panel-default">
						<div class="panel-heading clearfix">
							<h3 class="panel-title lh-2 pull-left">{{ Helper::getTaskGroup($id) }}</h3>
							<span class="pull-right mr-10">{{ link_to_action('QualityController@index', 'Cancel', array(), array('class' => 'btn btn-danger dib btn-sm')) }}</span>
						</div>
						<div class="panel-body">
							@if ($registrations->count())
							<table class="table table-striped table-condensed fs-13" id="datatable">
								<thead>
									<tr>
										<th>ID</th>
										<th>User</th>
										<th>Location</th>
										<th>Statuses</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($registrations as $registration)
									<tr>
										<td>{{ $registration->id }}</td>
										<td>{{ $registration->user['email'] }}</td>
										<td>{{ $registration->location['name'] }}</td>
										<td>@php foreach($registration->answer_statuses as $k=>$v) {echo $k.': '.$v."<br />";}@endphp</td>
										<td nowrap>
											{!! Html::decode(Html::linkAction('QualityController@edit', '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', [$registration->id, $status], array('class' => 'btn btn-info dib btn-sm'))) !!}
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
							@else
							There are no items to display
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endsection
	@push('styles')
	<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
	@endpush
	@push('scripts')
	<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript">
	$('#datatable').DataTable({
		columnDefs: [{orderable: false, targets: -1 }],
		order: [[ 0, "desc" ]],
    	stateSave: true
	});
	$('button.delete-btn').on('click', function(e){
		e.preventDefault();
		var self = $(this);
		swal({
			title             : "Are you sure?",
			text              : "You will not be able to recover this registration!",
			type              : "warning",
			showCancelButton  : true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText : "Yes, delete it!",
			cancelButtonText  : "No, Cancel",
			closeOnConfirm    : false,
			closeOnCancel     : false
		},
		function(isConfirm){
			if(isConfirm){
				self.parents("#delete_form").submit();
			}
			else{
				swal("Cancelled","Your registration is safe", "error");
			}
		});
	});
	</script>
	@endpush
