@extends('layouts.app')

@section('content')
<div class="main-full-back text-center vcenter min-height-100pc pt-20">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-left">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<h3 class="panel-title lh-2">
							Payment {{Html::linkAction('PaymentController@index', 'Cancel', $status, array('class' => 'btn btn-danger dib btn-sm pull-right text-white'))}}
						</h3>
					</div>
					<div class="panel-body">
						<h5 class="mt-0">Project: {{ $registration['taskgroup']->name }}</h5>
						<h5 class="mt-0 mb-0">User: {{ $registration['profile']->name }} {{ $registration['profile']->surname }} ({{ $registration['user']->email }})</h5>
					</div>
				</div>
				@if ($registration['answers'])
				{!! BootForm::open(array('action' => array('PaymentController@update', $registration->id, $status ))) !!}
				<div class="panel panel-default mb-20">
					@foreach ($registration['answers'] as $answer)
					<div class="panel-heading clearfix">
						<h5 class="mt-0 mb-0 lh-16">Question: {{$answer['description']->question}}
							{!! BootForm::hidden('answer_status_id['.$answer["id"].']') !!}
							<span class="btn btn-info btn-sm pull-right text-white">Money: {{$answer['money']}}</span>
						</h5>
					</div>
					@endforeach
					<div class="panel-body">
						<h5 class="mt-0 mb-0 lh-16">Total:
							<span class="btn btn-info btn-sm pull-right text-white">Money: {{$registration->total_money}}</span>
						</h5>
					</div>
					<div class="panel-footer">
						{!! BootForm::select('status', 'Status', $statuses, $registration->answer_status, ['required' => true]) !!}
					</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-body">
						{!! BootForm::submit('Submit',  ['class' => 'btn btn-primary pull-right text-white pull-right' ]) !!}
					</div>
				</div>
				{{ BootForm::close() }}
				@else
				There are no answers
				@endif
			</div>
		</div>
	</div>
</div>
</div>
@endsection
@push('styles')
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap2/bootstrap-switch.min.css" rel="stylesheet">
@endpush
@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key={!!env('GOOGLE_API_KEY') !!}&amp;libraries=places&amp;language=en-GB"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js"></script>
<script type="text/javascript">
$('.switch').bootstrapSwitch();
</script>
<script type="text/javascript">
$(':radio,:checkbox').click(function(){
	return false;
});
</script>
@endpush