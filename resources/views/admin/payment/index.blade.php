@extends('layouts.app')

@section('content')
<div class="main-full-back text-center vcenter min-height-100pc pt-20">
	<div class="container">
		<div class="row mb-20">
			<div class="col-md-5 col-md-offset-1 text-left">
				<ul class="list-inline">
					<li>{!! Html::decode(Html::linkAction('PaymentController@index', 'all', null)) !!}</li>
					@foreach ($statuses as $statuses)
					<li>{!! Html::decode(Html::linkAction('PaymentController@index', $statuses->status, [$statuses->status])) !!}</li>
					@endforeach
				</ul>
			</div>

			<div class="col-md-5 text-right">
				@if ($status)
				<ul class="list-inline">
					<li>{!! Html::decode(Html::linkAction('PaymentController@index', 'Individual', [$status])) !!}</li>
					<li>{!! Html::decode(Html::linkAction('PaymentController@index', 'Grouped', [$status, 'grouped'])) !!}</li>
				</ul>
				@endif
			</div>
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-left">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<h3 class="panel-title">Payment</h3>
					</div>
					<div class="panel-body">
						@if ($registrations->count())
						@if ($group && $status!='paid')
						{{ Form::open(array('method' => 'post','url' => array('admin/payment/bulk', $status))) }}
						@endif
						<table class="table table-striped table-condensed fs-13" id="datatable" style="width:100%;">
							<thead>
								<tr>
									<th>ID</th>
									<th>User</th>
									@if ($group)
									<th>IBAN</th>
									@else
									<th>Taskgroup</th>
									@endif
									<th>Money</th>
									@if ($group)
									<th>Bank</th>
									@endif
									<th>Status</th>
									@if ($status=='accepted')
									<th>Redeem</th>
									@endif
									@if ($status!='paid')
									<th>Actions</th>
									@endif
								</tr>
							</thead>
							<tbody>
								@foreach ($registrations as $registration)
								<tr>
									<td>{{ $registration->id }}</td>
									<td>{{ $registration->user['email'] }}</td>
									@if ($group)
									<td>{{ Helper::getUserProfile($registration->user_id)['IBAN'] }}</td>
									@else
									<td>{{ $registration->taskgroup['name'] }}</td>
									@endif
									<td>{{ $registration->answers_money }}</td>
									@if ($group)
									<td>{{ Helper::getBank(Helper::getUserProfile($registration->user_id)['bank_id']) }}</td>
									@endif
									<td>{{ $registration->answers_status }}</td>
									@if ($status=='accepted')
									<td>{{ $registration->redeem_status }}</td>
									@endif
									@if ($status!='paid')
									@if ($group)
									<td nowrap>
										{!! Form::checkbox('user_id[]', $registration->user_id, null, ['class'=> 'switch']) !!}
									</td>
									@else
									<td nowrap>
										{!! Html::decode(Html::linkAction('PaymentController@edit', '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', [$registration->id, $status], array('class' => 'btn btn-info dib btn-sm'))) !!}
									</td>
									@endif
									@endif
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<td></td>
									<th>User</th>
									@if ($group)
									<th>Bank</th>
									@else
									<th>Taskgroup</th>
									@endif
									<td><input id="min" type="text" placeholder="Min" size="6"></br><input id="max" type="text" placeholder="Max" size="6"></td>
									@if ($group)
									<th>Bank</th>
									@endif
									<th>Status</th>
									@if ($status=='accepted')
									<th>Redeem</th>
									@endif
									@if ($status!='paid')
									<td></td>
									@endif
								</tr>
							</tfoot>
						</table>
						@if ($group && $status!='paid')
						<div class="panel-heading clearfix">
							{!! Form::button('Move Status', array('type'=>'submit','class' => 'btn btn-info btn-sm pull-right')) !!}
							{{ Form::close() }}
						</div>
						@endif
						@else
						There are no items to display
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('styles')
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap2/bootstrap-switch.min.css" rel="stylesheet">
@endpush
@push('scripts')
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.24/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.24/daterangepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js"></script>
<script type="text/javascript">
$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var min = parseFloat( $('#min').val(), 10 );
        var max = parseFloat( $('#max').val(), 10 );
        var age = parseFloat( data[3] ) || 0; // use data for the age column
 
        if ( ( isNaN( min ) && isNaN( max ) ) ||
             ( isNaN( min ) && age <= max ) ||
             ( min <= age   && isNaN( max ) ) ||
             ( min <= age   && age <= max ) )
        {
            return true;
        }
        return false;
    }
);
$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#datatable tfoot th').each( function () {
    	var title = $(this).text();
    	$(this).html( '<input type="text" placeholder="Search '+title+'" size="10" id="fs"  />' );
    } );

    // DataTable
    var table = $('#datatable').DataTable({
    	dom: "<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>>" +
    	"<'row'<'col-sm-12'tr>>" +
    	"<'row'<'col-sm-5'i><'col-sm-7'p>>",
    	buttons: [
    	{
    		extend: "csv",
    		exportOptions: {
    			columns: [0, 1, 2, 3, 4, 5]
    		}
    	},
    	{
    		extend: "excel",
    		exportOptions: {
    			columns: [0, 1, 2, 3, 4, 5]
    		}
    	},
    	{
    		extend: "pdf",
    		exportOptions: {
    			columns: [0, 1, 2, 3, 4, 5]
    		}
    	},
    	{
    		extend: "print",
    		exportOptions: {
    			columns: [0, 1, 2, 3, 4, 5]
    		}
    	}
    	],
    	columnDefs: [{orderable: false, targets: -1 }],
    	order: [[ 0, "desc" ]],
    	stateSave: true
    });

    $('#min, #max').keyup( function() {
    	table.draw();
    } );
    // Apply the search
    table.columns().every( function () {
    	var that = this;

    	$( 'input#fs', this.footer() ).on( 'keyup change', function () {
    		if ( that.search() !== this.value ) {
    			that
    			.search( this.value )
    			.draw();
    		}
    	} );
    });

    // Date range script - Start of the sscript
    $("#date_start_range").daterangepicker({
    	autoUpdateInput: false,
    	locale: {
    		"cancelLabel": "Clear",
    	}
    });

    $("#date_start_range").on('apply.daterangepicker', function(ev, picker) {
    	$(this).val(picker.startDate.format('YYYY-MM-DD') + ' to ' + picker.endDate.format('YYYY-MM-DD'));
    	table.draw();
    });

    $("#date_start_range").on('cancel.daterangepicker', function(ev, picker) {
    	$(this).val('');
    	table.draw();
    });

});
$('.switch').bootstrapSwitch();
$('button.delete-btn').on('click', function(e){
	e.preventDefault();
	var self = $(this);
	swal({
		title             : "Are you sure?",
		text              : "You will not be able to recover this registration!",
		type              : "warning",
		showCancelButton  : true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText : "Yes, delete it!",
		cancelButtonText  : "No, Cancel",
		closeOnConfirm    : false,
		closeOnCancel     : false
	},
	function(isConfirm){
		if(isConfirm){
			self.parents("#delete_form").submit();
		}
		else{
			swal("Cancelled","Your registration is safe", "error");
		}
	});
});
</script>
@endpush
