@extends('layouts.app')

@section('content')
<div class="main-full-back text-center vcenter min-height-100pc pt-20">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-left">
                <div class="panel panel-default">
                    <div class="panel-heading clearfix">
                        <h3 class="panel-title lh-2">Project Status
                            <span class="pull-right">{{ link_to_route('admin.taskgroups.index', 'Cancel', array(), array('class' => 'btn btn-danger dib btn-sm')) }}</span>
                        </h3>
                    </div>
                    <div class="panel-body">
                        <table class="table table-striped">
                            <thead>
                                <th>Status</th>
                                <th>Action</th>
                            </thead>
                            <tbody>
                                <td>{{$status['state']}} </td>
                                <td>
                                    @if ($status['action'])
                                    {{ Form::open(array('method' => 'post','action' => array('TaskgroupsController@status', $taskgroup->id) )) }}
                                    {{ Form::hidden('action', $status['action']) }}
                                    {{ Form::button($status['action'], array('type' => 'submit', 'class' => 'btn btn-'.$status['btn'].' btn-xs')) }}
                                    {{ Form::close() }}
                                    @endif
                                </td>
                            </tbody>
                            @if($status['state'] == 'paused' || $status['state'] == 'completed')
                            <tfoot>
                                <th colspan='2'>
                                    @if($status['state'] == 'paused') 
                                    Paused on: {!! \Carbon\Carbon::parse($taskgroup->action_date)->format('d-m-Y h:i')  !!}
                                    @elseif($status['state'] == 'completed')
                                    Completed on: {!! \Carbon\Carbon::parse($taskgroup->action_date)->format('d-m-Y h:i') !!}
                                    @endif
                                </th>
                            </tfoot>
                            @endif
                        </table>
                    </div>
                </div>
                <div class="panel panel-default">
                 <div class="panel-heading clearfix">
                  <h3 class="panel-title lh-2">Edit Project
                    <span class="pull-right">{{ link_to_route('admin.taskgroups.index', 'Cancel', array(), array('class' => 'btn btn-danger dib btn-sm')) }}</span>
                </h3>
            </div>
            <div class="panel-body">
              @include('admin.partials._formTaskgroup')
          </div>
      </div>
  </div>
</div>
</div>
</div>
@endsection
@push('styles')
<link href="//cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.min.css" rel="stylesheet">
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.min.css" rel="stylesheet">
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/css/bootstrap-datetimepicker.min.css" rel="stylesheet">
<link href="//code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css" rel="stylesheet">
<link href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/css/bootstrap2/bootstrap-switch.min.css" rel="stylesheet">
@endpush
@push('scripts')
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-filestyle/1.2.1/bootstrap-filestyle.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/wysihtml/0.4.15/wysihtml5x-toolbar.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/handlebars.js/1.3.0/handlebars.runtime.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap3-wysiwyg/0.3.3/bootstrap3-wysihtml5.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.0/moment.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datetimepicker/4.17.43/js/bootstrap-datetimepicker.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-switch/3.3.2/js/bootstrap-switch.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/chosen/1.6.2/chosen.jquery.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/multiselect/2.2.9/js/multiselect.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jscolor/2.0.4/jscolor.min.js"></script>
<script>
$(".textarea").each(function(){$(this).wysihtml5();});
$('.switch').bootstrapSwitch();
$('.chosen').chosen();
$('#multiselect').multiselect({
    search: {
        left: '<input type="text" name="q" class="form-control" placeholder="Search..." />',
    }
});
$("#start_date").datetimepicker({
    defaultDate: "{!! $taskgroup->start_date_parse->format('Y-m-d H:i') !!}",
    format:'YYYY-MM-DD HH:mm'
});
$("#end_date").datetimepicker({
    defaultDate: "{!! $taskgroup->end_date_parse->format('Y-m-d H:i') !!}",
    format:'YYYY-MM-DD HH:mm'
});
$("#commission_date").datetimepicker({
    defaultDate: "{!! $taskgroup->commission_date_parse->format('Y-m-d H:i') !!}",
    format:'YYYY-MM-DD HH:mm'
});
$("#delivery_date").datetimepicker({
    defaultDate: "{!! $taskgroup->delivery_date_parse->format('Y-m-d H:i') !!}",
    format:'YYYY-MM-DD HH:mm'
});
$("#settlement_date").datetimepicker({
    defaultDate: "{!! $taskgroup->settlement_date_parse->format('Y-m-d H:i') !!}",
    format:'YYYY-MM-DD HH:mm'
});
$(function () {
	$(":file").filestyle();
});
$(document).ready(function() {
    var max_fields      = 10; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    
    
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        var rand = Math.floor(Math.random() * 10000000000);
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="row mb-10">'+
                '<div class="col-md-3"><input autocomplete="off" required="1" class="form-control" name="media_url['+rand+'][title]" placeholder="Media title" type="text"/></div>'+
                '<div class="col-md-3"><input autocomplete="off" required="1" class="form-control" name="media_url['+rand+'][url]" type="text" placeholder="Media URL" /></div>'+
                '<div class="col-md-2"><select autocomplete="off" required="1" class="form-control" id="type'+rand+'" name="media_url['+rand+'][type]"></select></div>'+
                '<div class="col-md-2"><select autocomplete="off" required="1" class="form-control" id="order'+rand+'" name="media_url['+rand+'][order]"></select></div>'+
                '<div class="col-md-2"><input autocomplete="off" required="1" name="media_url['+rand+'][is_active]" type="hidden" value="1" /><a href="#" class="remove_field input-group-addon">Remove</a></div>');
            //add input box
            var ts = document.getElementById('type'+rand);
            nots = document.createElement('option'); nots.innerHTML = 'Media Type'; ts.appendChild(nots);
            imgts = document.createElement('option'); imgts.value = 'image'; imgts.innerHTML = 'Image'; ts.appendChild(imgts);
            lnkts = document.createElement('option'); lnkts.value = 'link'; lnkts.innerHTML = 'Link'; ts.appendChild(lnkts);
            pdfts = document.createElement('option'); pdfts.value = 'pdf'; pdfts.innerHTML = 'PDF'; ts.appendChild(pdfts);

            var os = document.getElementById('order'+rand);
            noos = document.createElement('option'); noos.innerHTML = 'Media Order'; os.appendChild(noos);
            for (i = 1; i < 10; i++) {
                var osopt = document.createElement('option');
                osopt.value = i;
                osopt.innerHTML = i;
                os.appendChild(osopt);
            }
        }
    });

    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent().parent('div').remove(); x--;
    })
});
</script>
@endpush