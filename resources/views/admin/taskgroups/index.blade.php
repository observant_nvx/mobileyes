@extends('layouts.app')

@section('content')
<div class="main-full-back text-center vcenter min-height-100pc pt-20">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-left">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<h3 class="panel-title lh-2">All Projects
							<span class="pull-right lh-16">{{ link_to_route('admin.taskgroups.create', 'Add new project', array(), array('class' => 'btn btn-success dib btn-sm')) }}</span>
							<span class="pull-right lh-16 mr-20">{{ link_to_action('TaskgroupsController@duplicate', 'Duplicate project', array(), array('class' => 'btn btn-warning dib btn-sm')) }}</span>
						</h3>
					</div>
					<div class="panel-body fs-13">
						<div class="row mb-5">
							<span class="col-md-2">Toggle Data</span>
							<div class="list-inline col-md-10">
								<li><a class="toggle-vis" data-column="5">Color</a></li>
								<li><a class="toggle-vis" data-column="6">Commission Date</a></li>
								<li><a class="toggle-vis" data-column="7">Delivery Date</a></li>
								<li><a class="toggle-vis" data-column="8">Settlement Date</a></li>
							</div>
						</div>
					</div>
					<div class="panel-body">
						@if ($taskgroups->count())
						<table class="table table-striped table-condensed fs-13" id="datatable" style="width:100%;">
							<thead>
								<tr>
									<th>ID</th>
									<th>Image</th>
									<th>Name</th>
									<th>Start Date</th>
									<th>End Date</th>
									<th>Color</th>
									<th>Commission Date</th>
									<th>Delivery Date</th>
									<th>Settlement Date</th>
									<th>Status</th>
									<th>Actions</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($taskgroups as $taskgroup)
								<tr>
									<td>{{ $taskgroup->id }}</td>
									<td class="thumbnail">{{ Html::image(Helper::getTaskgroupImage($taskgroup->image), $taskgroup->name, array('class' => 'width-45 br-circle')) }}</td>
									<td>{{ $taskgroup->name }}</td>
									<td>{{ $taskgroup->start_date_parse->format('Y-m-d H:i') }}</td>
									<td>{{ $taskgroup->end_date_parse->format('Y-m-d H:i') }}</td>
									<td style="background:{{ $taskgroup->pin_color }}">&nbsp;</td>
									<td>{{ $taskgroup->commission_date_parse->format('Y-m-d H:i') }}</td>
									<td>{{ $taskgroup->delivery_date_parse->format('Y-m-d H:i') }}</td>
									<td>{{ $taskgroup->settlement_date_parse->format('Y-m-d H:i') }}</td>
									<td>{{ $taskgroup->status_text }}</td>
									<td nowrap>{!! Html::decode(link_to_route('admin.taskgroups.edit', '<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', array($taskgroup->id), array('class' => 'btn btn-info dib btn-xs'))) !!}
										<span class="dib">|</span>
										{!! Html::decode(link_to_action('TaskgroupsController@images', '<i class="fa fa-picture-o" aria-hidden="true"></i>', array($taskgroup->id), array('class' => 'btn btn-warning dib btn-xs'))) !!}
										<span class="dib">|</span>
										{!! Html::decode(link_to_action('TaskgroupsController@locations', '<i class="fa fa-tasks" aria-hidden="true"></i>', array($taskgroup->id), array('class' => 'btn btn-success dib btn-xs'))) !!}
										<span class="dib">|</span>
										{{ Form::open(array('method' => 'delete','id'=>'delete_form', 'route' => array('admin.taskgroups.destroy', $taskgroup->id), 'class' => 'dib' )) }}                       
										{!! Html::decode(Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i>', array('class' => 'btn btn-danger btn-xs delete-btn'))) !!}
										{{ Form::close() }}
									</td>
								</tr>
								@endforeach
							</tbody>
							<tfoot>
								<tr>
									<td></td>
									<td></td>
									<th>Name</th>
									<td><input id="date_start_range" type="text" placeholder="Select Range" size="12"></td>
									<td><input id="date_end_range" type="text" placeholder="Select Range" size="12"></td>
									<td></td>
									<td><input id="date_commission_range" type="text" placeholder="Select Range" size="12"></td>
									<td><input id="date_delivery_range" type="text" placeholder="Select Range" size="12"></td>
									<td><input id="date_settlement_range" type="text" placeholder="Select Range" size="12"></td>
									<td>{!! Form::select('status', $statuses, null, array('class'=>'form-control')) !!}</td>
									<td></td>
								</tr>
							</tfoot>
						</table>
						@else
						There are no taskgroups
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('styles')
<link href="https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css" rel="stylesheet">
<link href="https://cdn.datatables.net/buttons/1.2.2/css/buttons.dataTables.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.24/daterangepicker.css" rel="stylesheet">
@endpush
@push('scripts')
<script src="https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js"></script>
<script src="https://cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.2.2/js/buttons.print.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.24/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-daterangepicker/2.1.24/daterangepicker.min.js"></script>
<script type="text/javascript">
$(document).ready(function() {
    // Setup - add a text input to each footer cell
    $('#datatable tfoot th').each( function () {
    	var title = $(this).text();
    	$(this).html( '<input type="text" placeholder="Search '+title+'" size="12" id="fs"  />' );
    } );

    // DataTable
    var table = $('#datatable').DataTable({
    	dom: "<'row'<'col-sm-4'l><'col-sm-4'B><'col-sm-4'f>>" +
    	"<'row'<'col-sm-12'tr>>" +
    	"<'row'<'col-sm-5'i><'col-sm-7'p>>",
    	buttons: [
    	{
    		extend: "csv",
    		exportOptions: {
    			columns: [0, 1, 2, 3, 4, 5]
    		}
    	},
    	{
    		extend: "excel",
    		exportOptions: {
    			columns: [0, 1, 2, 3, 4, 5]
    		}
    	},
    	{
    		extend: "pdf",
    		exportOptions: {
    			columns: [0, 1, 2, 3, 4, 5]
    		}
    	},
    	{
    		extend: "print",
    		exportOptions: {
    			columns: [0, 1, 2, 3, 4, 5]
    		}
    	}
    	],
    	columnDefs: [{orderable: false, targets: [-1,1] }],
    	order: [[ 0, "desc" ]],
    	stateSave: true
    });

    $('#min, #max').keyup( function() {
    	table.draw();
    } );
    // Apply the search
    table.columns().every( function () {
    	var that = this;

    	$( 'input#fs, select', this.footer() ).on( 'keyup change', function () {
    		if ( that.search() !== this.value ) {
    			that
    			.search( this.value )
    			.draw();
    		}
    	} );
    });

    // Date range script - Start of the sscript
    $("#date_start_range").daterangepicker({
    	autoUpdateInput: false,
    	locale: {
    		"cancelLabel": "Clear",
    	}
    });

    $("#date_start_range").on('apply.daterangepicker', function(ev, picker) {
    	$(this).val(picker.startDate.format('YYYY-MM-DD') + ' to ' + picker.endDate.format('YYYY-MM-DD'));
    	table.draw();
    });

    $("#date_start_range").on('cancel.daterangepicker', function(ev, picker) {
    	$(this).val('');
    	table.draw();
    });

    // Date range script - Start of the sscript
    $("#date_end_range").daterangepicker({
    	autoUpdateInput: false,
    	locale: {
    		"cancelLabel": "Clear",
    	}
    });

    $("#date_end_range").on('apply.daterangepicker', function(ev, picker) {
    	$(this).val(picker.startDate.format('YYYY-MM-DD') + ' to ' + picker.endDate.format('YYYY-MM-DD'));
    	table.draw();
    });

    $("#date_end_range").on('cancel.daterangepicker', function(ev, picker) {
    	$(this).val('');
    	table.draw();
    });

    // Date range script - Start of the sscript
    $("#date_commission_range").daterangepicker({
    	autoUpdateInput: false,
    	locale: {
    		"cancelLabel": "Clear",
    	}
    });

    $("#date_commission_range").on('apply.daterangepicker', function(ev, picker) {
    	$(this).val(picker.startDate.format('YYYY-MM-DD') + ' to ' + picker.endDate.format('YYYY-MM-DD'));
    	table.draw();
    });

    $("#date_commission_range").on('cancel.daterangepicker', function(ev, picker) {
    	$(this).val('');
    	table.draw();
    });

    // Date range script - Start of the sscript
    $("#date_delivery_range").daterangepicker({
    	autoUpdateInput: false,
    	locale: {
    		"cancelLabel": "Clear",
    	}
    });

    $("#date_delivery_range").on('apply.daterangepicker', function(ev, picker) {
    	$(this).val(picker.startDate.format('YYYY-MM-DD') + ' to ' + picker.endDate.format('YYYY-MM-DD'));
    	table.draw();
    });

    $("#date_delivery_range").on('cancel.daterangepicker', function(ev, picker) {
    	$(this).val('');
    	table.draw();
    });

    // Date range script - Start of the sscript
    $("#date_settlement_range").daterangepicker({
    	autoUpdateInput: false,
    	locale: {
    		"cancelLabel": "Clear",
    	}
    });

    $("#date_settlement_range").on('apply.daterangepicker', function(ev, picker) {
    	$(this).val(picker.startDate.format('YYYY-MM-DD') + ' to ' + picker.endDate.format('YYYY-MM-DD'));
    	table.draw();
    });

    $("#date_settlement_range").on('cancel.daterangepicker', function(ev, picker) {
    	$(this).val('');
    	table.draw();
    });

    for (var i = 5; i < 9; i++) {
    	table.column( i ).visible( false );
    }

    $('a.toggle-vis').on( 'click', function (e) {
    	e.preventDefault();
        // Get the column API object
        var column = table.column( $(this).attr('data-column') );
        // Toggle the visibility
        column.visible( ! column.visible() );
    });
// Date range script - END of the script

$.fn.dataTableExt.afnFiltering.push(
	
	function( oSettings, aData, iDataIndex ) {
		var column = table.column( 3 );
		if($("#date_start_range").val()!='' && column.visible()) {
			var grab_daterange = $("#date_start_range").val();
			var give_results_daterange = grab_daterange.split(" to ");
			var filterstart = give_results_daterange[0];
			var filterend = give_results_daterange[1];
		    var iStartDateCol = 3; //using column 2 in this instance
		    var iEndDateCol = 3;
		    var tabledatestart = aData[iStartDateCol];
		    var tabledateend= aData[iEndDateCol];

		    if ( !filterstart && !filterend )
		    {
		    	return true;
		    }
		    else if ((moment(filterstart).isSame(tabledatestart) || moment(filterstart).isBefore(tabledatestart)) && filterend === "")
		    {
		    	return true;
		    }
		    else if ((moment(filterstart).isSame(tabledatestart) || moment(filterstart).isAfter(tabledatestart)) && filterstart === "")
		    {
		    	return true;
		    }
		    else if ((moment(filterstart).isSame(tabledatestart) || moment(filterstart).isBefore(tabledatestart)) && (moment(filterend).isSame(tabledateend) || moment(filterend).isAfter(tabledateend)))
		    {
		    	return true;
		    }
		    return false;
		}
		return true;
	}
);

$.fn.dataTableExt.afnFiltering.push(
	
	function( oSettings, aData, iDataIndex ) {
		var column = table.column( 4 );
		if($("#date_end_range").val()!='' && column.visible()) {
			var grab_daterange = $("#date_end_range").val();
			var give_results_daterange = grab_daterange.split(" to ");
			var filterstart = give_results_daterange[0];
			var filterend = give_results_daterange[1];
		    var iStartDateCol = 4; //using column 2 in this instance
		    var iEndDateCol = 4;
		    var tabledatestart = aData[iStartDateCol];
		    var tabledateend= aData[iEndDateCol];

		    if ( !filterstart && !filterend )
		    {
		    	return true;
		    }
		    else if ((moment(filterstart).isSame(tabledatestart) || moment(filterstart).isBefore(tabledatestart)) && filterend === "")
		    {
		    	return true;
		    }
		    else if ((moment(filterstart).isSame(tabledatestart) || moment(filterstart).isAfter(tabledatestart)) && filterstart === "")
		    {
		    	return true;
		    }
		    else if ((moment(filterstart).isSame(tabledatestart) || moment(filterstart).isBefore(tabledatestart)) && (moment(filterend).isSame(tabledateend) || moment(filterend).isAfter(tabledateend)))
		    {
		    	return true;
		    }
		    return false;
		}
		return true;
	}
);

$.fn.dataTableExt.afnFiltering.push(
	
	function( oSettings, aData, iDataIndex ) {
		var column = table.column( 6 );
		if($("#date_commission_range").val()!='' && column.visible()) {
			var grab_daterange = $("#date_commission_range").val();
			var give_results_daterange = grab_daterange.split(" to ");
			var filterstart = give_results_daterange[0];
			var filterend = give_results_daterange[1];
		    var iStartDateCol = 6; //using column 2 in this instance
		    var iEndDateCol = 6;
		    var tabledatestart = aData[iStartDateCol];
		    var tabledateend= aData[iEndDateCol];

		    if ( !filterstart && !filterend )
		    {
		    	return true;
		    }
		    else if ((moment(filterstart).isSame(tabledatestart) || moment(filterstart).isBefore(tabledatestart)) && filterend === "")
		    {
		    	return true;
		    }
		    else if ((moment(filterstart).isSame(tabledatestart) || moment(filterstart).isAfter(tabledatestart)) && filterstart === "")
		    {
		    	return true;
		    }
		    else if ((moment(filterstart).isSame(tabledatestart) || moment(filterstart).isBefore(tabledatestart)) && (moment(filterend).isSame(tabledateend) || moment(filterend).isAfter(tabledateend)))
		    {
		    	return true;
		    }
		    return false;
		}
		return true;
	}
);


$.fn.dataTableExt.afnFiltering.push(
	
	function( oSettings, aData, iDataIndex ) {
		var column = table.column( 7 );
		if($("#date_delivery_range").val()!='' && column.visible()) {
			var grab_daterange = $("#date_delivery_range").val();
			var give_results_daterange = grab_daterange.split(" to ");
			var filterstart = give_results_daterange[0];
			var filterend = give_results_daterange[1];
		    var iStartDateCol = 7; //using column 2 in this instance
		    var iEndDateCol = 7;
		    var tabledatestart = aData[iStartDateCol];
		    var tabledateend= aData[iEndDateCol];

		    if ( !filterstart && !filterend )
		    {
		    	return true;
		    }
		    else if ((moment(filterstart).isSame(tabledatestart) || moment(filterstart).isBefore(tabledatestart)) && filterend === "")
		    {
		    	return true;
		    }
		    else if ((moment(filterstart).isSame(tabledatestart) || moment(filterstart).isAfter(tabledatestart)) && filterstart === "")
		    {
		    	return true;
		    }
		    else if ((moment(filterstart).isSame(tabledatestart) || moment(filterstart).isBefore(tabledatestart)) && (moment(filterend).isSame(tabledateend) || moment(filterend).isAfter(tabledateend)))
		    {
		    	return true;
		    }
		    return false;
		}
		return true;
	}
);


$.fn.dataTableExt.afnFiltering.push(
	
	function( oSettings, aData, iDataIndex ) {
		var column = table.column( 8 );
		if($("#date_settlement_range").val()!='' && column.visible()) {
			var grab_daterange = $("#date_settlement_range").val();
			var give_results_daterange = grab_daterange.split(" to ");
			var filterstart = give_results_daterange[0];
			var filterend = give_results_daterange[1];
		    var iStartDateCol = 8; //using column 2 in this instance
		    var iEndDateCol = 8;
		    var tabledatestart = aData[iStartDateCol];
		    var tabledateend= aData[iEndDateCol];

		    if ( !filterstart && !filterend )
		    {
		    	return true;
		    }
		    else if ((moment(filterstart).isSame(tabledatestart) || moment(filterstart).isBefore(tabledatestart)) && filterend === "")
		    {
		    	return true;
		    }
		    else if ((moment(filterstart).isSame(tabledatestart) || moment(filterstart).isAfter(tabledatestart)) && filterstart === "")
		    {
		    	return true;
		    }
		    else if ((moment(filterstart).isSame(tabledatestart) || moment(filterstart).isBefore(tabledatestart)) && (moment(filterend).isSame(tabledateend) || moment(filterend).isAfter(tabledateend)))
		    {
		    	return true;
		    }
		    return false;
		}
		return true;
	}
);

});
$('button.delete-btn').on('click', function(e){
	e.preventDefault();
	var self = $(this);
	swal({
		title             : "Are you sure?",
		text              : "This Project will be deleted!",
		type              : "warning",
		showCancelButton  : true,
		confirmButtonColor: "#DD6B55",
		confirmButtonText : "Yes, deleted it!",
		cancelButtonText  : "No, Cancel",
		closeOnConfirm    : false,
		closeOnCancel     : false
	},
	function(isConfirm){
		if(isConfirm){
			self.parents("#delete_form").submit();
		}
		else{
			swal("Cancelled","Your Project is safe", "error");
		}
	});
});
</script>
@endpush
