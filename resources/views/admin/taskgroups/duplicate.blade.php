@extends('layouts.app')

@section('content')
<div class="main-full-back text-center vcenter min-height-100pc pt-20">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-left">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<h3 class="panel-title lh-2">Select Project</h3>
					</div>
					
					<div class="panel-body form-group clearfix">
						{!! BootForm::open(['action' => 'TaskgroupsController@post_duplicate', 'method' => 'post', 'class' => 'duplicate_form']) !!}
						{!! BootForm::select('taskgroup', 'Project', $taskgroups, null, ['required' => true, 'autocomplete' => 'off', 'class' => 'form-control']) !!}
						{!! BootForm::submit('Submit',  ['class' => 'duplicate-btn btn btn-primary pull-right text-white pull-right' ]) !!}
						{{ BootForm::close() }}
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
$('.duplicate-btn').on('click', function(e){
	e.preventDefault();
	var self = $(this);
	swal({
		title             : "Are you sure?",
		text              : "This Project will be duplicated!",
		type              : "info",
		showCancelButton  : true,
		confirmButtonColor: "#5bc0de",
		confirmButtonText : "Yes, duplicated it!",
		cancelButtonText  : "No, Cancel",
		closeOnConfirm    : false,
		closeOnCancel     : false
	},
	function(isConfirm){
		if(isConfirm){
			self.parents(".duplicate_form").submit();
		}
		else{
			swal("Cancelled","Your Project is not duplicated", "error");
		}
	});
});
</script>
@endpush