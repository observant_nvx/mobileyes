@extends('layouts.app')

@section('content')
<div class="main-full-back text-center vcenter min-height-100pc pt-20">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-left">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<h3 class="panel-title lh-16">
							Activity: {{ $taskgroup->name }} {{Html::linkAction('TaskgroupsController@index', 'Cancel', null, array('class' => 'btn btn-danger dib btn-sm pull-right text-white'))}}
						</h3>
					</div>
					<div class="panel-body">
						@if ($taskgroup->active_locations->count())
						<table class="table table-striped table-condensed fs-13" id="datatable" style="width:100%;">
							<thead>
								<tr>
									<th>Name</th>
									<th>Address</th>
									<th>Status</th>
									<th>User</th>
									<th>Time</th>
								</tr>
							</thead>
							<tbody>
								@foreach ($taskgroup->active_locations as $location)
								<tr>
									<td>{{ $location->name }}</td>
									<td>{{ $location->adddress }}</td>
									<td>@if ($location->reg_status) {{ Helper::getRegistrationStatusById($location->reg_status) }} @else - @endif</td>
									<td>@if ($location->reg_user) {{ Helper::getUser($location->reg_user) }} @else - @endif</td>
									<td>@if ($location->reg_time) {{ $location->reg_time->format('m/d/Y H:i') }} @else - @endif</td>
								</tr>
								@endforeach
							</tbody>
						</table>
						@else
						There is no activity
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection