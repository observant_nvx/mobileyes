@extends('layouts.app')

@section('content')
<div class="main-full-back text-center vcenter min-height-100pc pt-20">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-left">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<h3 class="panel-title lh-16">
							Images {{Html::linkAction('TaskgroupsController@index', 'Cancel', null, array('class' => 'btn btn-danger dib btn-sm pull-right text-white'))}}
						</h3>
					</div>
					<div class="panel-body">
						<h5 class="mt-0">Project: {{ $taskgroup->name }}</h5>
						
					</div>
				</div>
				
				@if ($tasks->count())
				@foreach ($tasks as $task)
				<div class="panel panel-default mb-20">
					<div class="panel-heading clearfix">
						<h5 class="mt-0 mb-0 lh-2 col-md-5 pl-0">Question: {{$task['description']->question}}</h5>
					</div>
					<div class="panel-body row ml-0 mr-0">
						@if ($task->answers->count())
						@foreach ($task->answers as $answer)
						<div class="col-md-3 panel panel-default text-center">
							<div class="panel-body pl-0 pr-0 pb-0">{{ Html::image('images/answers/'.$answer['data'], $task['description']->question, array('class' => 'img-responsive')) }}</div>
							<div class="panel-footer mb-10 pl-5 pr-5 fs-13">
								<strong>User</strong>: {{ Helper::getProfileFromAnswer($answer['user_id'])['name'] }} {{ Helper::getProfileFromAnswer($answer['user_id'])['surname'] }}<br />
								<strong>Address</strong>: {{ Helper::getLocationFromRegistration($answer['registration_id'])['name'] }} ({{ Helper::getLocationFromRegistration($answer['registration_id'])['address'] }})<br />
								@if($answer['created_at'])<strong>Time</strong>: {{ $answer['created_at']->format('m/d/Y H:i') }}@endif
							</div>
						</div>
						@endforeach
						@else
						There are no multimedia files uploaded yet
						@endif
					</div>
				</div>
				@endforeach
				@else
				<div class="panel panel-default mb-20">
					<div class="panel-heading clearfix">
						<h5 class="mt-0 mb-0 lh-2 col-md-5 pl-0">There are no multimedia jobs</h5>
					</div>
				</div>	
				@endif
			</div>
		</div>
	</div>
</div>
@endsection