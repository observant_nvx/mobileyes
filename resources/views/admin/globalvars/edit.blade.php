@extends('layouts.app')

@section('content')
<div class="main-full-back text-center vcenter min-height-100pc pt-20">
	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1 text-left">
				<div class="panel panel-default">
					<div class="panel-heading clearfix">
						<h3 class="panel-title lh-16">Edit Answer <a href="{{ URL::route('admin.answers.index') }}" class="btn btn-danger pull-right text-white">Cancel</a></h3>
					</div>
                    <div class="panel-body">
                        <h5 class="mt-0">Project: {{ Helper::getTaskGroup($answers[0]['taskgroup_id']) }}</h5>
                        <h5 class="mt-0 mb-0">User: {{ Helper::getUser($answers[0]['user_id']) }}</h5>
                    </div>
                </div>
                @if ($answers)
                {!! BootForm::open(['action' => 'AnswersController@store']) !!}
                @foreach ($answers as $answer)
                <div class="panel panel-default mb-20">
                    <div class="panel-heading clearfix">
                        <h5 class="mt-0 mb-0 lh-16">Question: {{$answer['description']->question}}
                            <span class="btn btn-info btn-sm pull-right text-white">Money: {{$answer['money']}}</span></h5>
                        </div>
                        <div class="panel-body">
                            @if($answer['tasktype_id']==4)
                            {!! BootForm::textarea(null, 'Answer', $answer['data'], ['readonly' => true, 'rows' => 3, ]) !!}
                            @elseif ($answer['tasktype_id']==6)
                            @php $coord = explode(",",substr($answer['data'], 1, -1));@endphp
                            {!! BootForm::text(null, 'Latitude', $coord[0], ['readonly' => true ]) !!}
                            {!! BootForm::text(null, 'Longnitude', $coord[1], ['readonly' => true ]) !!}
                            @elseif ($answer['tasktype_id']==1)
                            @php $answer['data'] =  intval($answer['data']); @endphp
                            {!! BootForm::radios(null, 'Answers', $answer['description']->answers, $answer['data'], ['disabled' => true ]) !!}
                            @elseif ($answer['tasktype_id']==2)
                            @php $answer['data'] =  explode(',',$answer['data']); @endphp
                            {!! BootForm::checkboxes(null, 'Answers', $answer['description']->answers, $answer['data'], ['disabled' => true ]) !!}
                            @endif
                        </div>
                        <div class="panel-footer">
                            {!! BootForm::select('answer_status_id['.$answer["id"].']', 'Answer Status', $answer_statuses, $answer['answer_status_id'], ['required' => true]) !!}
                        </div>
                    </div>
                    @endforeach
                    <div class="panel panel-default">
                        <div class="panel-body">
                            {!! BootForm::submit('Submit',  ['class' => 'btn btn-primary pull-right text-white pull-right' ]) !!}
                        </div>
                    </div>
                    {!! BootForm::close() !!}
                    @else
                    There are no answers
                    @endif
                </div>
            </div>
        </div>
    </div>
    @endsection
    @push('scripts')
    <script type="text/javascript">
    $(':radio,:checkbox').click(function(){
        return false;
    });
    </script>
    @endpush

