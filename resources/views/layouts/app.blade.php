<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>MobilEyes - @yield('title')</title>
    <link href="{{asset('css/app.css') }}" rel="stylesheet">
    <link href="{{asset('css/all.css') }}" rel="stylesheet">
    <link href="{{asset('css/style.css') }}" rel="stylesheet">
    <link href="{{asset('css/overrides.css') }}" rel="stylesheet">
    @stack('styles')
</head>
<body class="">

    <nav class="navbar navbar-default navbar-white navbar-static-top navbar-background mb-0 no-b">
        <div class="container">
            <div class="navbar-header text-center col-md-0">

                <!-- Collapsed Hamburger -->
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                    <span class="sr-only">Toggle Navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Branding Image -->
                <a class="navbar-brand auto-height" href="{{ url('/') }}">
                    {{ Html::image('images/logo.png', 'Mobileyes', array('class' => 'logo')) }}
                </a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right mb-0 mr-0 text-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                    <li class="pr-10 pl-sm-15"><a class="bw-1 dpad bs-s br-black text-black" href="{{ url('/login') }}"><i class="fa fa-sign-in hidden-xs hidden-md hidden-lg" aria-hidden="true"></i> <span class="hidden-sm">Login</span></a></li>
                    <li class="pl-sm-15"><a class="bw-1 bs-s dpad bc-purple br-light-purple text-white" href="{{ url('/register') }}"><i class="fa fa-pencil-square-o hidden-xs hidden-md hidden-lg" aria-hidden="true"></i> <span class="hidden-sm">Register</span></a></li>
                    @else
                    <li class="pt-15 pr-15">{{ Auth::user()->email }}</li>
                    <li><a href="{{ url('/admin/clients') }}">Clients</a></li>
                    <li><a href="{{ url('/admin/locations') }}">Locations</a></li>
                    <li><a href="{{ url('/admin/taskgroups') }}">Projects</a></li>
                    <li><a href="{{ url('/admin/tasks') }}">Jobs</a></li>
                    <li><a href="{{ url('/admin/quality') }}">Quality</a></li>
                    <li><a href="{{ url('/admin/payment') }}">Payment</a></li>
                    <li><a href="{{ url('/admin/users') }}">Users</a></li>
                    <li><a href="{{ url('/admin/globalvars') }}">Global Vars</a></li>
                    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    <div class="wrapper flex-center position-ref full-height">
        @yield('content')
    </div>
    <div class="copyright">
        <div class="container pt-20 pb-20">
            <div class="row">
                <div class="col-md-12">MobilEyes Ltd&trade; 2016</div>
            </div>
        </div>
    </div>
    
    <!-- JavaScripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/all.js') }}"></script>
    @stack('scripts')
    @include('sweet::alert')
</body>
</html>
