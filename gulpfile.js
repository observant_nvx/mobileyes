var elixir = require('laravel-elixir');
require('laravel-elixir-remove');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
	mix.copy(
		'node_modules/bootstrap-sass/assets/fonts/bootstrap/',
		'public/fonts/bootstrap'
		); 

	mix.copy(
 		'node_modules/font-awesome/fonts',
 		'public/fonts'
 		);

	/* sweetalert */
	mix.copy(
 		'node_modules/sweetalert/dist/sweetalert.css',
 		'resources/assets/css/lib'
 		);
	mix.copy(
 		'node_modules/sweetalert/dist/sweetalert.min.js',
 		'resources/assets/js/lib'
 		);

	mix.browserify('app.js');

    mix.sass('app.scss')
    .styles([
 		'lib/sweetalert.css',
 		],  'public/css/all.css')
 	.scripts([
 		'lib/sweetalert.min.js',
 		],  'public/js/all.js');

 	mix.version(['public/css/app.css','public/js/app.js','public/js/all.js']);
    
});
